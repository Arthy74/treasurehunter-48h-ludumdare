module.exports = function(grunt) {

    grunt.loadNpmTasks('grunt-force');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks("grunt-ts");

    var banner = [
            '/**',
            ' * <%= pkg.name %> - v<%= pkg.version %>',
            ' * <%= pkg.homepage %>',
            ' *',
            ' * Compiled: <%= grunt.template.today("yyyy-mm-dd") %>',
            ' *',
            ' */',
            ''
        ].join('\n');

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        dirs: {
            build: 'bin',
            src: 'src/'
        },

        files: {
            srcFiles: '<%= dirs.src %>/**/*.js',
            srcAllJS: '**/*.js',
            build: '<%= dirs.build %>/game.dev.js',
            buildMin: '<%= dirs.build %>/game.js'
        },

        uglify: {
            options: {
                banner: banner
            },
            dist: {
                src: '<%= files.build %>',
                dest: '<%= files.buildMin %>'
            }
        },

        watch: {
            build: {

                files: ['<%= dirs.src %>/**/*.js'],
                tasks: ['force:on','ts:basic', 'uglify'],
                options: {
                    interrupt: true
                }
            }
        },

        ts: {
            // Set the default options, see : http://gruntjs.com/configuring-tasks#options
            options: {
                // 'es3' (default) | 'es5'
                target: 'es3',
                // Use amd for asynchonous loading or commonjs  'amd' (default) | 'commonjs'
                module: 'commonjs',
                // Generate a source map file for each result js file (true (default) | false)
                sourcemap: true,
                // Generate a declaration .d.ts file for each resulting js file (true | false  (default))
                declaration: false,
                // ??? (true | false (default))
                nolib: false,
                // Leave comments in compiled js code (true | false (default))
                comments: false,
                // Print the tsc command (true | false (default))
                verbose: true
            },
            basic: {
                test: false,
                options: {
                    sourcemap: true,
                    declaration: true,
                    fast:'always'
                },
                src: ['<%= dirs.src %>/**/*.ts'],
                out: 'bin/game.dev.js'
            }
        }
    });

    grunt.task.registerTask('compile', ['force:on','ts:basic', 'uglify']);
};