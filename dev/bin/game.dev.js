var log;
(function (log) {
    var Logger = (function () {
        function Logger() {
        }
        Logger.init = function (verbose) {
            Logger.verbose = verbose;
            Logger.start = Date.now();
        };

        Logger.log = function () {
            var args = [];
            for (var _i = 0; _i < (arguments.length - 0); _i++) {
                args[_i] = arguments[_i + 0];
            }
            if (!Logger.verbose)
                return;

            var logString = (Date.now() - Logger.start) + ") " + args.toString();
            if (this.logLayer != null) {
                this.logHistory.unshift(logString + "\n");
                if (this.logHistory.length > this.logHistoryMax)
                    this.logHistory.pop();

                this.logText.setText(this.logHistory.toString());
            } else {
                console.log(logString);
            }
        };

        Logger.addOnscreenLogger = function (container) {
            this.logLayer = new PIXI.DisplayObjectContainer();
            this.logBG = new PIXI.Graphics();
            this.logLayer.addChild(this.logBG);

            container.addChild(this.logLayer);

            this.logHistory = new Array();

            this.onChangeScale();
        };

        Logger.onChangeScale = function () {
            if (this.logLayer == null)
                return;
            this.redraw(0, 0, Math.round(window.innerWidth / 3), Math.round(window.innerHeight));
        };

        Logger.redraw = function (xx, yy, ww, hh) {
            if (this.logText != null) {
                this.logLayer.removeChild(this.logText);
                this.logText = null;
            }

            this.logBG.clear();
            this.logBG.beginFill(0x000000, 0.5);
            this.logBG.drawRect(xx, yy, ww, hh);

            var font = { font: "12px Verdana", fill: "#FFFFFF", align: "left" };
            font.wordWrap = true;
            font.wordWrapWidth = ww - 50;

            this.logText = new PIXI.Text("", font);
            this.logText.width = ww;

            this.logLayer.addChild(this.logText);

            console.log(xx, yy, ww, hh);
        };
        Logger.logHistoryMax = 50;

        Logger.verbose = false;
        return Logger;
    })();
    log.Logger = Logger;
})(log || (log = {}));
var util;
(function (util) {
    var Clock = (function () {
        function Clock() {
        }
        Clock.init = function () {
            this._start = Date.now();
        };
        Clock.getTimer = function () {
            return (Date.now() - Clock._start);
        };
        Clock.SIMPLE_CLICK = 100;
        return Clock;
    })();
    util.Clock = Clock;
})(util || (util = {}));
var util;
(function (util) {
    (function (signal) {
        var SignalCallback = (function () {
            function SignalCallback(fCB, iPriority) {
                this.cb = fCB;
                this.priority = iPriority;
            }
            return SignalCallback;
        })();
        signal.SignalCallback = SignalCallback;
    })(util.signal || (util.signal = {}));
    var signal = util.signal;
})(util || (util = {}));
var util;
(function (util) {
    (function (signal) {
        var ASignal = (function () {
            function ASignal(id_message, verbose) {
                if (typeof verbose === "undefined") { verbose = false; }
                this.callbacks = new Array();

                this.id = id_message;
                this.verbose = verbose;
            }
            ASignal.prototype.dispose = function () {
                this.callbacks = new Array();
                this.callbacks = null;
            };

            ASignal.prototype.addCallback = function (cb, priority) {
                if (typeof priority === "undefined") { priority = 0; }
                this._idx = this.getItemIndex(cb);

                if (this._idx == -1) {
                    this.callbacks.push(new signal.SignalCallback(cb, priority));

                    this.callbacks.sort(this.sortCallBack);
                }
            };

            ASignal.prototype.sortCallBack = function (a, b) {
                if (a.priority < b.priority)
                    return 1;
                else if (a.priority > b.priority)
                    return -1;
                return 0;
            };

            ASignal.prototype.removeCallback = function (cb) {
                this._idx = this.getItemIndex(cb);

                if (this._idx != -1) {
                    this.callbacks.splice(this._idx, 1);
                }
            };

            ASignal.prototype.removeALLCallbacks = function () {
                this.callbacks = new Array();
            };

            ASignal.prototype.exists = function (cb) {
                return (this.getItemIndex(cb) != -1);
            };

            ASignal.prototype.invokeCallbacks = function (args) {
                if (this.verbose)
                    log.Logger.log("invoking callbacks '" + this.id + "'  nb:" + this.callbacks.length + "  nb args:" + args.length);

                var a = this.callbacks.concat();
                var l = a.length;
                var cb;

                for (var i = 0; i < l; i++) {
                    cb = a[i].cb;

                    if (cb == null) {
                        log.Logger.log("WARNING on " + this.id + ": callback may be not available anymore or callback signature is not the same (number of arguments expected:" + args.length + "). Removing callback.");
                        this.removeCallback(cb);
                        continue;
                    }

                    switch (args.length) {
                        case 0:
                            cb.call(null);
                            break;
                        case 1:
                            cb.call(null, args[0]);
                            break;
                        case 2:
                            cb.call(null, args[0], args[1]);
                            break;
                        case 3:
                            cb.call(null, args[0], args[1], args[2]);
                            break;
                        case 4:
                            cb.call(null, args[0], args[1], args[2], args[3]);
                            break;
                        case 5:
                            cb.call(null, args[0], args[1], args[2], args[3], args[4]);
                            break;
                        case 6:
                            cb.call(null, args[0], args[1], args[2], args[3], args[4], args[5]);
                            break;
                        case 7:
                            cb.call(null, args[0], args[1], args[2], args[3], args[4], args[5], args[6]);
                            break;
                        case 8:
                            cb.call(null, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7]);
                            break;
                        case 9:
                            cb.call(null, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8]);
                            break;
                        case 10:
                            cb.call(null, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9]);
                            break;
                        case 11:
                            cb.call(null, args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10]);
                            break;
                        default:
                    }
                }
            };

            ASignal.prototype.toString = function () {
                return "nb of callbacks for message '" + this.id + "' : " + this.callbacks.length;
            };

            ASignal.prototype.getItemIndex = function (ref) {
                var idx = -1;
                var l = this.callbacks.length;
                for (var i = 0; i < l; i++) {
                    if (this.callbacks[i].cb == ref) {
                        return i;
                        break;
                    }
                }
                return idx;
            };
            return ASignal;
        })();
        signal.ASignal = ASignal;
    })(util.signal || (util.signal = {}));
    var signal = util.signal;
})(util || (util = {}));
var util;
(function (util) {
    (function (signal) {
        var SignalManager = (function () {
            function SignalManager() {
            }
            SignalManager.init = function () {
                if (SignalManager._signals == null)
                    SignalManager._signals = new Array();
            };

            SignalManager.setVerbose = function (b) {
                SignalManager._verbose = b;
                for (var z in this._signals) {
                    SignalManager._signals[z].verbose = b;
                }
            };

            SignalManager.getVerbose = function () {
                return this._verbose;
            };

            SignalManager.clearAll = function () {
                for (var z in SignalManager._signals) {
                    SignalManager._signals[z].dispose();
                    SignalManager._signals[z] = null;
                    delete SignalManager._signals[z];
                }
            };

            SignalManager.existCallback = function (signalID, cb) {
                if (!SignalManager._signals[signalID])
                    return false;
                return SignalManager._signals[signalID].exists(cb);
            };

            SignalManager.addCallbackSignal = function (signalID, cb, priority) {
                if (typeof priority === "undefined") { priority = 0; }
                if (!SignalManager._signals[signalID]) {
                    SignalManager._signals[signalID] = new signal.ASignal(signalID, SignalManager._verbose);
                }

                SignalManager._signals[signalID].addCallback(cb, priority);
            };

            SignalManager.removeCallbackSignal = function (signalID, cb) {
                if (SignalManager._signals[signalID] == null)
                    return;

                SignalManager._signals[signalID].removeCallback(cb);

                if (SignalManager._signals[signalID].callbacks.length == 0) {
                    delete SignalManager._signals[signalID];
                }
            };

            SignalManager.invokeSignal = function (signalID) {
                var args = [];
                for (var _i = 0; _i < (arguments.length - 1); _i++) {
                    args[_i] = arguments[_i + 1];
                }
                if (!SignalManager._signals[signalID])
                    return;

                if (SignalManager._verbose)
                    log.Logger.log(" -- invoking Signal '" + signalID + "'callbacks	" + this._signals[signalID].callbacks.length);

                SignalManager._signals[signalID].invokeCallbacks(args);
            };

            SignalManager.listSignal = function () {
                log.Logger.log("---------- list Signals (START) --------");

                for (var z in SignalManager._signals) {
                    log.Logger.log("	" + SignalManager._signals[z].toString());
                }
                log.Logger.log("---------- list Signals (END) --------");
            };
            SignalManager._verbose = false;
            return SignalManager;
        })();
        signal.SignalManager = SignalManager;
    })(util.signal || (util.signal = {}));
    var signal = util.signal;
})(util || (util = {}));
var util;
(function (util) {
    (function (signal) {
        var CoreSignals = (function () {
            function CoreSignals() {
            }
            CoreSignals.ON_ENTERFRAME = "on_enterframe";

            CoreSignals.CONFIG_LOADED = "config loaded";

            CoreSignals.ASSETS_PROGRESS = "assets progress";
            CoreSignals.ASSETS_LOADED = "assets loaded";

            CoreSignals.SHOW_REQUEST_SHIELD = "show request shield";
            CoreSignals.HIDE_REQUEST_SHIELD = "hide request shield";
            return CoreSignals;
        })();
        signal.CoreSignals = CoreSignals;
    })(util.signal || (util.signal = {}));
    var signal = util.signal;
})(util || (util = {}));
var game;
(function (game) {
    (function (signal) {
        var Signals = (function () {
            function Signals() {
            }
            Signals.DEFAULT_BUTTON_CLICK = "default_click";

            Signals.REACHED_EXIT = "reached_exit";
            return Signals;
        })();
        signal.Signals = Signals;
    })(game.signal || (game.signal = {}));
    var signal = game.signal;
})(game || (game = {}));
var util;
(function (util) {
    var DisposeUtil = (function () {
        function DisposeUtil() {
        }
        DisposeUtil.dispose = function (container) {
            for (var i = 0, len = container.children.length; i < len; i++) {
                if (container.children[i] instanceof PIXI.DisplayObjectContainer)
                    DisposeUtil.dispose(container.children[i]);

                if (container.children[i] instanceof PIXI.Text)
                    container.children[i].destroy(true);
            }
        };

        DisposeUtil.destroyTexture = function (txt) {
        };
        DisposeUtil.disposeText = function (txt) {
            txt.destroy(true);
        };
        return DisposeUtil;
    })();
    util.DisposeUtil = DisposeUtil;
})(util || (util = {}));
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var game;
(function (game) {
    var DisposeUtil = util.DisposeUtil;

    var AScreen = (function (_super) {
        __extends(AScreen, _super);
        function AScreen() {
            _super.call(this);
            this.id = "ascreen";

            this.internalID = AScreen.COUNT++;

            this.built = false;
            this.paused = false;
        }
        AScreen.prototype.rescale = function (sc) {
            this.scale = new PIXI.Point(sc, sc);
        };

        AScreen.prototype.onRemoved = function () {
            DisposeUtil.dispose(this);
            this.removeChildren();
        };

        AScreen.prototype.onAdded = function () {
        };

        AScreen.prototype.pause = function () {
            this.paused = true;
        };

        AScreen.prototype.resume = function () {
            this.paused = false;
        };

        AScreen.prototype.isPaused = function () {
            return this.paused;
        };
        AScreen.COUNT = 0;
        return AScreen;
    })(PIXI.DisplayObjectContainer);
    game.AScreen = AScreen;
})(game || (game = {}));
var util;
(function (util) {
    (function (ui) {
        var CSS = (function () {
            function CSS() {
            }
            CSS.getFont = function (id, size, color, align) {
                if (typeof align === "undefined") { align = "left"; }
                return { font: String(size) + "px " + id, fill: color, align: align };
            };

            CSS.addDropShadow = function (obj, color, angle, distance) {
                if (typeof angle === "undefined") { angle = Math.PI / 4; }
                if (typeof distance === "undefined") { distance = 3; }
                obj.dropShadow = true;
                obj.dropShadowColor = color;
                obj.dropShadowAngle = angle;
                obj.dropShadowDistance = distance;
            };

            CSS.addStroke = function (obj, color, thickness) {
                obj.stroke = color;
                obj.strokeThickness = thickness;
            };

            CSS.alignInRect = function (txt, rect, alignX, alignY) {
                switch (alignX) {
                    case "center":
                        txt.position.x = Math.round(rect.x + (rect.width - txt.width) / 2);
                        break;
                    case "left":
                        txt.position.x = Math.round(rect.x);
                        break;
                    case "right":
                        txt.position.x = Math.round(rect.x + rect.width - txt.width);
                        break;
                }
                switch (alignY) {
                    case "middle":
                    case "center":
                        txt.position.y = Math.round(rect.y + (rect.height - txt.height) / 2);
                        break;
                    case "top":
                        txt.position.y = Math.round(rect.y);
                        break;
                    case "bottom":
                        txt.position.y = Math.round(rect.y + rect.height - txt.height);
                        break;
                }
            };
            CSS.FONT_CandelaBold = "CandelaBold";
            CSS.FONT_Pusab = "Pusab";
            return CSS;
        })();
        ui.CSS = CSS;
    })(util.ui || (util.ui = {}));
    var ui = util.ui;
})(util || (util = {}));
var util;
(function (util) {
    (function (ui) {
        var SignalManager = util.signal.SignalManager;
        var Signals = game.signal.Signals;

        var CommonButton = (function (_super) {
            __extends(CommonButton, _super);
            function CommonButton(baseTxtName, overTxtName, active) {
                if (typeof active === "undefined") { active = true; }
                var _this = this;
                _super.call(this);
                this.buttonValue = "";
                this.buttonFamily = "";
                this.buttonIndex = -1;
                this.onMouseUp = function (data) {
                    if (!_this._wasDown || data.target != _this)
                        return;

                    data.originalEvent.stopPropagation();

                    _this._over.alpha = 0;
                    _this._wasDown = false;

                    _this.clickData = data;

                    SignalManager.invokeSignal(_this._clickSignal, _this);
                };

                this._clickSignal = Signals.DEFAULT_BUTTON_CLICK;

                this.tween = new TWEEN.Tween(this);
                this.tween.easing(TWEEN.Easing.Cubic.Out);

                this.textureOn = PIXI.Texture.fromFrame(baseTxtName);

                this.selectedReplaceOver = false;

                this._base = PIXI.Sprite.fromFrame(baseTxtName);
                this._over = PIXI.Sprite.fromFrame(overTxtName);

                this._base.anchor.x = this._base.anchor.y = 0.5;
                this._over.anchor.x = this._over.anchor.y = 0.5;

                this.addChild(this._base);
                this.addChild(this._over);

                this.setActive(active);

                this._over.alpha = 0;
            }
            CommonButton.prototype.disposeLabel = function () {
                if (this._label != null) {
                    this.removeChild(this._label);
                    this._label.destroy(true);
                    this._label = null;
                }
            };

            CommonButton.prototype.setLabel = function (value) {
                this.disposeLabel();

                if (value == "")
                    return;

                var font = ui.CSS.getFont(ui.CSS.FONT_Pusab, 50, "#555555");
                ui.CSS.addStroke(font, "#aaaaaa", 4);

                this._label = new PIXI.Text(value, font);
                this.addChild(this._label);

                this._label.anchor.x = this._label.anchor.y = 0.5;
            };

            CommonButton.prototype.dispose = function () {
                this.setActive(false);
                this.disposeLabel();
                this.clickData = null;
                this._over = null;
                this._selectedSprite = null;
                this._offExtra = null;
                this._base = null;

                if (this.textureOn != null) {
                    util.DisposeUtil.destroyTexture(this.textureOn);
                    this.textureOn = null;
                }
                if (this.textureOn != null) {
                    util.DisposeUtil.destroyTexture(this.textureOff);
                    this.textureOff = null;
                }

                if (this.tween != null)
                    this.tween.stop();
                this.tween = null;
            };

            CommonButton.prototype.setBaseTexture = function (txt) {
                this._base.texture = txt;
            };

            CommonButton.prototype.setOverTexture = function (txt) {
                this._over.texture = txt;
            };

            CommonButton.prototype.offsetOver = function (xx, yy) {
                this._over.position.x = xx;
                this._over.position.y = yy;
            };

            CommonButton.prototype.setOffTexture = function (offTxt) {
                this._offExtra = new PIXI.Sprite(offTxt);
                this._offExtra.anchor.x = this._offExtra.anchor.y = 0.5;
                this.addChild(this._offExtra);
            };
            CommonButton.prototype.offsetOffTexture = function (xx, yy) {
                this._offExtra.position.x = xx;
                this._offExtra.position.y = yy;
            };

            CommonButton.prototype.setSignal = function (id) {
                this._clickSignal = id;
            };

            CommonButton.prototype.setActive = function (b, alterTextures) {
                if (typeof alterTextures === "undefined") { alterTextures = true; }
                if (b)
                    this._active = true;
                else
                    this._active = false;

                this.updateButtonState(b, alterTextures);
            };

            CommonButton.prototype.updateButtonState = function (b, alterTextures) {
                if (typeof alterTextures === "undefined") { alterTextures = true; }
                this.buttonMode = b;
                this.interactive = b;

                if (b) {
                    this.alpha = 1;

                    this.mouseover = this.onMouseOver;
                    this.mouseout = this.onMouseOut;
                    this.mousedown = this.touchstart = this.onMouseDown;
                    this.mouseup = this.mouseupoutside = this.touchend = this.touchendoutside = this.onMouseUp;

                    if (alterTextures) {
                        if (this.textureOff != null) {
                            this._base.texture = this.textureOn;
                        }
                        if (this._offExtra != null) {
                            this._offExtra.visible = false;
                        }
                    }
                } else {
                    this.mouseover = null;
                    this.mouseout = null;
                    this.mousedown = null;
                    this.mouseup = this.mouseupoutside = this.touchend = this.touchendoutside = null;

                    if (alterTextures) {
                        if (this.textureOff != null) {
                            this._base.texture = this.textureOff;
                        }
                        if (this._offExtra != null) {
                            this._offExtra.visible = true;
                            this.addChild(this._offExtra);
                        }
                    }
                }
            };

            CommonButton.prototype.select = function (b) {
                this._selected = b;
                this.updateSelectedState();
            };

            CommonButton.prototype.updateSelectedState = function () {
                if (this.selectedReplaceOver) {
                    this._selectedSprite.alpha = this._selected ? 1 : 0;
                    this._base.alpha = this._selected ? 0 : 1;
                    this._over.alpha = 0;
                } else {
                    if (this._selected) {
                        if (this._selectedSprite != null)
                            this._selectedSprite.alpha = 1;
                        else if (this._over != null)
                            this._over.alpha = 1;
                    } else {
                        if (this._selectedSprite != null)
                            this._selectedSprite.alpha = 0;
                        if (this._over != null)
                            this._over.alpha = 0;
                    }
                }
            };

            CommonButton.prototype.onMouseOver = function (data) {
                this.defaultCursor = "pointer";
                data.originalEvent.stopPropagation();

                if (!this._selected) {
                    this._over.alpha = 1;
                }
            };

            CommonButton.prototype.onMouseOut = function (data) {
                this.defaultCursor = "pointer";
                if (data.originalEvent != null)
                    data.originalEvent.stopPropagation();

                this.updateSelectedState();
            };

            CommonButton.prototype.onMouseDown = function (data) {
                this._wasDown = true;
                data.originalEvent.stopPropagation();
                this._over.alpha = 1;
            };
            return CommonButton;
        })(PIXI.DisplayObjectContainer);
        ui.CommonButton = CommonButton;
    })(util.ui || (util.ui = {}));
    var ui = util.ui;
})(util || (util = {}));
var game;
(function (game) {
    (function (model) {
        (function (enums) {
            var ScreenEnum = (function () {
                function ScreenEnum() {
                }
                ScreenEnum.LOADING = "loading";
                ScreenEnum.MAIN = "main";
                ScreenEnum.INGAME = "ingame";
                ScreenEnum.END_GAME = "end_game";
                return ScreenEnum;
            })();
            enums.ScreenEnum = ScreenEnum;
        })(model.enums || (model.enums = {}));
        var enums = model.enums;
    })(game.model || (game.model = {}));
    var model = game.model;
})(game || (game = {}));
var util;
(function (util) {
    (function (io) {
        var Logger = log.Logger;
        var SignalManager = util.signal.SignalManager;
        var CoreSignals = util.signal.CoreSignals;

        var IORequest = (function () {
            function IORequest(url, method, data) {
                if (typeof method === "undefined") { method = "get"; }
                if (typeof data === "undefined") { data = null; }
                var _this = this;
                this.onGetAnswer = function (evt) {
                    var json = JSON.parse(_this._request.responseText);

                    if (!_this.silenceMode) {
                        SignalManager.invokeSignal(CoreSignals.HIDE_REQUEST_SHIELD);
                    }

                    if (_this._success != null)
                        _this._success.call(null, json);
                };
                this.onError = function (evt) {
                    log.Logger.log("---> ON ERROR ", evt);
                    if (!_this.silenceMode) {
                        SignalManager.invokeSignal(CoreSignals.HIDE_REQUEST_SHIELD);
                    }
                    if (_this._fail != null)
                        _this._fail.call(null, evt.type);
                };
                this.silenceMode = false;

                this._method = method;
                this._url = url;
                this._data = data;

                this._request = new XMLHttpRequest();
                this._request.onload = this.onGetAnswer;
                this._request.onerror = this.onError;
                this._request.ontimeout = this.onError;

                Logger.log(this._request);
                this._request.open(this._method, this._url, true);
            }
            IORequest.prototype.sendRequest = function (cbSuccess, cbfail) {
                if (typeof cbfail === "undefined") { cbfail = null; }
                if (!this.silenceMode) {
                    SignalManager.invokeSignal(CoreSignals.SHOW_REQUEST_SHIELD);
                }

                this._success = cbSuccess;
                this._fail = cbfail;

                if (this._method == "get") {
                    log.Logger.log("send request to ", this._url, "GET");
                    this._request.send();
                }

                if (this._method == "post") {
                    var data = JSON.stringify(this._data);
                    log.Logger.log("send request to ", this._url, "POST", data);
                    this._request.send(data);
                }
            };
            return IORequest;
        })();
        io.IORequest = IORequest;
    })(util.io || (util.io = {}));
    var io = util.io;
})(util || (util = {}));
var game;
(function (game) {
    (function (model) {
        (function (vo) {
            var GameVO = (function () {
                function GameVO() {
                }
                GameVO.prototype.setPredictiveMode = function (num, randomWinRate) {
                    switch (num) {
                        case 0:
                            this.playerShouldWin = (Math.random() < randomWinRate);
                            break;
                        case 1:
                            this.playerShouldWin = true;
                            break;
                        case 2:
                            this.playerShouldWin = false;
                            break;
                    }
                };
                return GameVO;
            })();
            vo.GameVO = GameVO;
        })(model.vo || (model.vo = {}));
        var vo = model.vo;
    })(game.model || (game.model = {}));
    var model = game.model;
})(game || (game = {}));
var game;
(function (game) {
    (function (model) {
        (function (vo) {
            var UserVO = (function () {
                function UserVO() {
                }
                return UserVO;
            })();
            vo.UserVO = UserVO;
        })(model.vo || (model.vo = {}));
        var vo = model.vo;
    })(game.model || (game.model = {}));
    var model = game.model;
})(game || (game = {}));
var game;
(function (game) {
    (function (model) {
        var UserVO = game.model.vo.UserVO;

        var UserModel = (function () {
            function UserModel() {
            }
            UserModel.prototype.init = function (data) {
                this._user = new UserVO();
                this._user.name = data.name;
                this._user.credits = data.credits;
            };

            UserModel.prototype.winCoins = function (num) {
                this._user.credits += num;
            };

            UserModel.prototype.noCredits = function () {
                return (this._user.credits == 0);
            };

            UserModel.prototype.getCredits = function () {
                return this._user.credits;
            };

            UserModel.prototype.useCoin = function () {
                this._user.credits--;
            };
            return UserModel;
        })();
        model.UserModel = UserModel;
    })(game.model || (game.model = {}));
    var model = game.model;
})(game || (game = {}));
var game;
(function (game) {
    (function (model) {
        var IORequest = util.io.IORequest;
        var SignalManager = util.signal.SignalManager;
        var CoreSignals = util.signal.CoreSignals;

        var Config = (function () {
            function Config() {
            }
            Config.init = function () {
                Config.USER = new model.UserModel();

                var timestamp = new Date().getTime();
                var req = new IORequest("assets/config.json?t=" + timestamp);
                req.sendRequest(Config.onGetConfigSuccess, Config.onGetConfigFail);
            };
            Config.GAME_WIDTH = 640;
            Config.GAME_HEIGHT = 960;

            Config.onGetConfigSuccess = function (json) {
                log.Logger.log("JSON ", json);

                Config.DATA = json;
                Config.IS_MOBILE = false;

                Config.USER.init(json.user);

                SignalManager.invokeSignal(CoreSignals.CONFIG_LOADED);
            };

            Config.onGetConfigFail = function (data) {
                log.Logger.log("error loading config ", data);
            };
            return Config;
        })();
        model.Config = Config;
    })(game.model || (game.model = {}));
    var model = game.model;
})(game || (game = {}));
var game;
(function (game) {
    (function (screen) {
        var Config = game.model.Config;
        var Title = (function (_super) {
            __extends(Title, _super);
            function Title() {
                _super.call(this, PIXI.Texture.fromFrame("title_top.png"));

                this.anchor = new PIXI.Point(0.5, 0.5);
                this.position = new PIXI.Point(Math.round(Config.GAME_WIDTH / 2), 80);
            }
            return Title;
        })(PIXI.Sprite);
        screen.Title = Title;
    })(game.screen || (game.screen = {}));
    var screen = game.screen;
})(game || (game = {}));
var game;
(function (game) {
    (function (screen) {
        var Config = game.model.Config;
        var CSS = util.ui.CSS;

        var Credits = (function (_super) {
            __extends(Credits, _super);
            function Credits() {
                _super.call(this);

                var coin = PIXI.Sprite.fromFrame("coin.png");
                coin.anchor = new PIXI.Point(0.5, 0.5);
                this.addChild(coin);

                var font = CSS.getFont(CSS.FONT_Pusab, 50, "#ffffff");
                CSS.addStroke(font, "#f99803", 4);

                this._creditsTF = new PIXI.Text(String(Config.USER.getCredits()), font);
                this._creditsTF.anchor.x = 0;
                this._creditsTF.anchor.y = 0.5;
                this._creditsTF.position = new PIXI.Point(50, 0);
                this.addChild(this._creditsTF);

                this.position = new PIXI.Point(50, Config.GAME_HEIGHT - 50);
            }
            Credits.prototype.update = function (animated) {
                if (typeof animated === "undefined") { animated = false; }
                this._creditsTF.setText(String(Config.USER.getCredits()));
            };
            return Credits;
        })(PIXI.DisplayObjectContainer);
        screen.Credits = Credits;
    })(game.screen || (game.screen = {}));
    var screen = game.screen;
})(game || (game = {}));
var util;
(function (util) {
    (function (audio) {
        var ASoundEngine = (function () {
            function ASoundEngine() {
                this.audioPath = "./assets/audio/";
            }
            ASoundEngine.prototype.init = function (audioJson, isMobile) {
            };

            ASoundEngine.prototype.playSound = function (id) {
            };

            ASoundEngine.prototype.muteSounds = function (b) {
            };
            return ASoundEngine;
        })();
        audio.ASoundEngine = ASoundEngine;
    })(util.audio || (util.audio = {}));
    var audio = util.audio;
})(util || (util = {}));
var util;
(function (util) {
    (function (audio) {
        (function (howler) {
            var HowlerSoundVO = (function () {
                function HowlerSoundVO(data, formats) {
                    this.id = data.id;
                    this.url = data.url;
                    this.preload = data.preload;
                    this.autoplay = data.autoplay;
                    this.volume = data.volume;
                    this.loop = data.loop;
                    this.formats = formats;
                }
                return HowlerSoundVO;
            })();
            howler.HowlerSoundVO = HowlerSoundVO;
        })(audio.howler || (audio.howler = {}));
        var howler = audio.howler;
    })(util.audio || (util.audio = {}));
    var audio = util.audio;
})(util || (util = {}));
var util;
(function (util) {
    (function (audio) {
        (function (howler) {
            var HowlerSound = (function () {
                function HowlerSound(engine, data) {
                    var _this = this;
                    this.onSoundComplete = function () {
                        log.Logger.log("onSoundComplete ", _this);
                    };
                    this.onSoundLoaded = function () {
                        _this._engineRef.onSoundPreloaded();
                    };
                    this._engineRef = engine;
                    this.data = data;

                    this._soundBuilt = false;
                    this._isMuted = false;

                    this._aSrc = [];
                    for (var i = 0, len = data.formats.length; i < len; i++) {
                        this._aSrc.push(this._engineRef.audioPath + data.url + "." + data.formats[i]);
                    }

                    if (this.data.preload)
                        this.buildSound();
                }
                HowlerSound.prototype.buildSound = function () {
                    this._soundInstance = new Howl({
                        src: this._aSrc,
                        autoplay: this.data.autoplay,
                        loop: this.data.loop,
                        volume: this.data.volume / 100,
                        onload: this.onSoundLoaded,
                        onend: this.onSoundComplete
                    });

                    this._soundBuilt = true;
                };

                HowlerSound.prototype.stop = function () {
                    this._soundInstance.stop();
                };

                HowlerSound.prototype.play = function () {
                    if (!this.data.preload && !this._soundBuilt)
                        this.buildSound();
                    this._soundInstance.play();

                    this.updateMuted();
                };

                HowlerSound.prototype.mute = function (b) {
                    log.Logger.log("mute ", b);
                    this._isMuted = b;
                    this.updateMuted();
                };

                HowlerSound.prototype.updateMuted = function () {
                    if (this._soundInstance == null)
                        return;

                    if (this._isMuted)
                        this._soundInstance.mute(true);
                    else
                        this._soundInstance.mute(false);
                };
                return HowlerSound;
            })();
            howler.HowlerSound = HowlerSound;
        })(audio.howler || (audio.howler = {}));
        var howler = audio.howler;
    })(util.audio || (util.audio = {}));
    var audio = util.audio;
})(util || (util = {}));
var util;
(function (util) {
    (function (audio) {
        (function (howler) {
            var Logger = log.Logger;

            var HowlerEngine = (function (_super) {
                __extends(HowlerEngine, _super);
                function HowlerEngine() {
                    _super.apply(this, arguments);
                }
                HowlerEngine.prototype.init = function (audioJson, isMobile) {
                    Logger.log("INIT Howler JS");

                    if (isMobile) {
                        this._formats = ['ogg', 'mp3'];
                    } else {
                        this._formats = ['mp3', 'ogg'];
                    }

                    this._HowlerSounds = new Array();
                    this._loadingCount = 0;

                    for (var i = 0, len = audioJson.length; i < len; i++) {
                        this._HowlerSounds[audioJson[i].id] = new howler.HowlerSound(this, new howler.HowlerSoundVO(audioJson[i], this._formats));
                        if (audioJson[i].preload)
                            this._loadingCount++;

                        Logger.log("added ", this._HowlerSounds[audioJson[i].id], this._loadingCount);
                    }

                    if (this._loadingCount == 0) {
                        audio.SoundManager.onEngineReady();
                    }
                };

                HowlerEngine.prototype.muteSounds = function (b) {
                    Logger.log("muteSounds ", b);
                    for (var z in this._HowlerSounds) {
                        this._HowlerSounds[z].mute(b);
                    }
                };

                HowlerEngine.prototype.onSoundPreloaded = function () {
                    Logger.log("onSoundPreloaded ", this._loadingCount);
                    this._loadingCount--;
                    if (this._loadingCount == 0) {
                        audio.SoundManager.onEngineReady();
                    }
                };

                HowlerEngine.prototype.playSound = function (id) {
                    Logger.log("Howler playing ", id);
                    this._HowlerSounds[id].play();

                    return this._HowlerSounds[id];
                };
                return HowlerEngine;
            })(audio.ASoundEngine);
            howler.HowlerEngine = HowlerEngine;
        })(audio.howler || (audio.howler = {}));
        var howler = audio.howler;
    })(util.audio || (util.audio = {}));
    var audio = util.audio;
})(util || (util = {}));
var util;
(function (util) {
    (function (audio) {
        (function (signal) {
            var SoundSignals = (function () {
                function SoundSignals() {
                }
                SoundSignals.SOUND_MUTED = "sound muted";
                SoundSignals.SOUNDS_READY = "sounds ready";
                return SoundSignals;
            })();
            signal.SoundSignals = SoundSignals;
        })(audio.signal || (audio.signal = {}));
        var signal = audio.signal;
    })(util.audio || (util.audio = {}));
    var audio = util.audio;
})(util || (util = {}));
var util;
(function (util) {
    (function (audio) {
        var SignalManager = util.signal.SignalManager;
        var SoundSignals = util.audio.signal.SoundSignals;
        var HowlerEngine = util.audio.howler.HowlerEngine;

        var SoundManager = (function () {
            function SoundManager() {
            }
            SoundManager.init = function (audioJson, isMobile) {
                this._ready = false;

                this._engine = new HowlerEngine();
                this._engine.init(audioJson, isMobile);

                SignalManager.addCallbackSignal(SoundSignals.SOUND_MUTED, this.onMuteSound);
            };

            SoundManager.onEngineReady = function () {
                this._ready = true;
                SignalManager.invokeSignal(SoundSignals.SOUNDS_READY);
            };

            SoundManager.setMobile = function () {
                this._ready = true;
            };

            SoundManager.playGameBG = function (id) {
                if (id == this._playingBGID)
                    return;

                if (this._playingBG)
                    this._playingBG.stop();

                this._playingBG = this.playSound(id);
                this._playingBGID = id;
            };

            SoundManager.playSound = function (id) {
                if (!SoundManager._ready)
                    return;
                return this._engine.playSound(id);
            };
            SoundManager._ready = false;

            SoundManager.onMuteSound = function (b) {
                SoundManager._engine.muteSounds(b);
            };
            return SoundManager;
        })();
        audio.SoundManager = SoundManager;
    })(util.audio || (util.audio = {}));
    var audio = util.audio;
})(util || (util = {}));
var game;
(function (game) {
    (function (screen) {
        var CSS = util.ui.CSS;
        var CommonButton = util.ui.CommonButton;
        var Signals = game.signal.Signals;
        var SignalManager = util.signal.SignalManager;

        var ScreenEnum = game.model.enums.ScreenEnum;
        var Config = game.model.Config;
        var SoundManager = util.audio.SoundManager;

        var MainScreen = (function (_super) {
            __extends(MainScreen, _super);
            function MainScreen() {
                _super.call(this);
                this.onButtonClick = function (buttonRef) {
                    if (buttonRef.buttonFamily == "start_bt") {
                        game.GameStage.goToScreen(ScreenEnum.INGAME);
                    }
                };
                this.id = "main";
            }
            MainScreen.prototype.onRemoved = function () {
                SignalManager.removeCallbackSignal(Signals.DEFAULT_BUTTON_CLICK, this.onButtonClick);

                this._startBt.dispose();
                this._startBt = null;

                _super.prototype.onRemoved.call(this);
            };

            MainScreen.prototype.onAdded = function () {
                var bg = PIXI.Sprite.fromImage("./assets/images/intro_bg.jpg");
                this.addChild(bg);

                var title = new screen.Title();
                this.addChild(title);

                var treasure = PIXI.Sprite.fromFrame("Treasure_big.png");
                treasure.anchor = new PIXI.Point(0.5, 0.5);
                treasure.position = new PIXI.Point(Math.round(Config.GAME_WIDTH / 2), 320);
                this.addChild(treasure);

                var font = CSS.getFont(CSS.FONT_Pusab, 30, "#ffffff");
                CSS.addStroke(font, "#272e26", 8);
                font.wordWrap = true;
                font.wordWrapWidth = 522;

                var help = new PIXI.Text(Config.DATA.texts.help, font);
                help.anchor.x = 0.5;
                help.position = new PIXI.Point(Math.round(Config.GAME_WIDTH / 2), 450);
                this.addChild(help);

                this._startBt = new CommonButton("button_out.png", "button_over.png");
                this._startBt.buttonFamily = "start_bt";
                this._startBt.position = new PIXI.Point(Math.round(Config.GAME_WIDTH / 2), 700);
                this._startBt.setLabel("START");
                this.addChild(this._startBt);

                var credits = new screen.Credits();
                this.addChild(credits);

                if (Config.USER.noCredits()) {
                    this._startBt.setActive(false);
                    this._startBt.alpha = 0.4;

                    var font = CSS.getFont(CSS.FONT_Pusab, 25, "#ffffff");
                    CSS.addStroke(font, "#272e26", 8);
                    font.wordWrap = true;
                    font.wordWrapWidth = 522;

                    var noCreditsTxt = new PIXI.Text(Config.DATA.texts.no_credits, font);
                    noCreditsTxt.anchor.x = 0.5;
                    noCreditsTxt.position = new PIXI.Point(Math.round(Config.GAME_WIDTH / 2), 780);
                    this.addChild(noCreditsTxt);
                } else {
                    this._startBt.setActive(true);
                    SignalManager.addCallbackSignal(Signals.DEFAULT_BUTTON_CLICK, this.onButtonClick);
                }

                SoundManager.playGameBG("intro_loop");
                _super.prototype.onAdded.call(this);
            };
            return MainScreen;
        })(game.AScreen);
        screen.MainScreen = MainScreen;
    })(game.screen || (game.screen = {}));
    var screen = game.screen;
})(game || (game = {}));
var util;
(function (util) {
    (function (ui) {
        var CoreSignals = util.signal.CoreSignals;
        var SignalManager = util.signal.SignalManager;

        var AssetsManager = (function () {
            function AssetsManager() {
            }
            AssetsManager.loadAssets = function (aSpriteSheets) {
                var assetsToLoad = new Array();
                for (var i = 0, len = aSpriteSheets.length; i < len; i++) {
                    assetsToLoad.push("./assets/spritesheets/" + aSpriteSheets[i].id);
                }

                assetsToLoad.push("./assets/images/bg0.jpg");
                assetsToLoad.push("./assets/images/bg1.jpg");
                assetsToLoad.push("./assets/images/bg2.jpg");
                assetsToLoad.push("./assets/images/bg3.jpg");
                assetsToLoad.push("./assets/images/intro_bg.jpg");
                assetsToLoad.push("./assets/images/end_bg_loose.jpg");
                assetsToLoad.push("./assets/images/end_bg.jpg");

                this._assetsLoader = new PIXI.AssetLoader(assetsToLoad, false);
                this._assetsLoader.addEventListener("onComplete", AssetsManager.onAssetsLoaded);
                this._assetsLoader.addEventListener("onProgress", AssetsManager.onAssetsProgress);

                this._assetsLoader.load();
            };

            AssetsManager.onAssetsProgress = function (event) {
                var p = 1 - (event.content.content.loadCount / event.content.content.assetURLs.length);
                SignalManager.invokeSignal(CoreSignals.ASSETS_PROGRESS, p);
            };

            AssetsManager.onAssetsLoaded = function () {
                SignalManager.invokeSignal(CoreSignals.ASSETS_LOADED);
            };
            return AssetsManager;
        })();
        ui.AssetsManager = AssetsManager;
    })(util.ui || (util.ui = {}));
    var ui = util.ui;
})(util || (util = {}));
var game;
(function (game) {
    (function (screen) {
        var Logger = log.Logger;
        var CSS = util.ui.CSS;
        var Config = game.model.Config;
        var SignalManager = util.signal.SignalManager;
        var SoundManager = util.audio.SoundManager;
        var SoundSignals = util.audio.signal.SoundSignals;
        var CoreSignals = util.signal.CoreSignals;
        var AssetsManager = util.ui.AssetsManager;
        var ScreenEnum = game.model.enums.ScreenEnum;

        var LoadingScreen = (function (_super) {
            __extends(LoadingScreen, _super);
            function LoadingScreen() {
                var _this = this;
                _super.call(this);
                this.onConfigLoaded = function () {
                    Logger.log("onConfigLoaded ");

                    SignalManager.removeCallbackSignal(CoreSignals.CONFIG_LOADED, _this.onConfigLoaded);

                    SignalManager.addCallbackSignal(CoreSignals.ASSETS_LOADED, _this.onAssetsLoaded);
                    SignalManager.addCallbackSignal(CoreSignals.ASSETS_PROGRESS, _this.onAssetsProgress);
                    AssetsManager.loadAssets(Config.DATA.spritesheets);
                };
                this.onAssetsProgress = function (percent) {
                    Logger.log("onAssetsProgress " + percent);

                    if (!isNaN(percent)) {
                        percent = Math.round(percent * 800) / 10;
                        _this._progress.setText(String(percent) + "%");
                    }
                };
                this.onAssetsLoaded = function () {
                    _this._progress.setText("80%");

                    SoundManager.init(Config.DATA.audio, Config.IS_MOBILE);
                    SignalManager.addCallbackSignal(SoundSignals.SOUNDS_READY, _this.onSoundsReady);

                    if (Config.IS_MOBILE) {
                        SoundManager.setMobile();
                        _this.onSoundsReady();
                    }
                };
                this.onSoundsReady = function () {
                    SignalManager.removeCallbackSignal(SoundSignals.SOUNDS_READY, _this.onSoundsReady);
                    _this._progress.setText("100%");

                    game.GameStage.goToScreen(ScreenEnum.MAIN);
                };
                this.id = "loading";
            }
            LoadingScreen.prototype.onAdded = function () {
                var font = CSS.getFont(CSS.FONT_Pusab, 50, "#cd3937");
                font.wordWrap = true;
                font.wordWrapWidth = 522;

                var title = new PIXI.Text("Loading", font);
                title.anchor.x = 0.5;
                title.position = new PIXI.Point(Math.round(Config.GAME_WIDTH / 2), 200);
                this.addChild(title);

                this._progress = new PIXI.Text("", font);
                this._progress.anchor.x = 0.5;
                this._progress.position = new PIXI.Point(Math.round(Config.GAME_WIDTH / 2), 250);
                this.addChild(this._progress);

                SignalManager.addCallbackSignal(CoreSignals.CONFIG_LOADED, this.onConfigLoaded);
                Config.init();
            };

            LoadingScreen.prototype.onRemoved = function () {
                SignalManager.removeCallbackSignal(CoreSignals.CONFIG_LOADED, this.onConfigLoaded);
                _super.prototype.onRemoved.call(this);

                this._progress = null;
            };
            return LoadingScreen;
        })(game.AScreen);
        screen.LoadingScreen = LoadingScreen;
    })(game.screen || (game.screen = {}));
    var screen = game.screen;
})(game || (game = {}));
var game;
(function (game) {
    (function (model) {
        var Level = (function () {
            function Level(xx, yy, ww, hh) {
                this.setDimension(xx, yy, ww, hh);
            }
            Level.prototype.setDimension = function (xx, yy, ww, hh) {
                this.rect = new PIXI.Rectangle(xx, yy, ww, hh);
                this.center = new PIXI.Point(Math.round(xx + ww / 2), Math.round(yy + hh / 2));
                this.generate();
            };

            Level.prototype.generate = function () {
                var margin = this.rect.width * 0.1;
                var xx;
                var yy;

                if (Math.random() > 0.5) {
                    xx = this.rect.x + margin + Math.random() * (this.rect.width - 2 * margin);
                    if (Math.random() > 0.5)
                        yy = this.rect.y + margin;
                    else
                        yy = this.rect.y + this.rect.height - margin;
                } else {
                    yy = this.rect.y + margin + Math.random() * (this.rect.height - 2 * margin);
                    if (Math.random() > 0.5)
                        xx = this.rect.x + margin;
                    else
                        xx = this.rect.x + this.rect.width - margin;
                }

                this.exit = new PIXI.Point(xx, yy);
            };
            return Level;
        })();
        model.Level = Level;
    })(game.model || (game.model = {}));
    var model = game.model;
})(game || (game = {}));
var game;
(function (game) {
    (function (model) {
        (function (vo) {
            var BehaviorDirectionVO = (function () {
                function BehaviorDirectionVO() {
                }
                BehaviorDirectionVO.prototype.randomize = function (firstValue) {
                    this.direction = Math.round(Math.random() * 3);

                    if (firstValue) {
                        this.stepDuration = Math.round(2 + Math.random() * 5);
                    } else {
                        this.stepDuration = Math.round(32 + Math.random() * 64);
                    }
                };
                return BehaviorDirectionVO;
            })();
            vo.BehaviorDirectionVO = BehaviorDirectionVO;
        })(model.vo || (model.vo = {}));
        var vo = model.vo;
    })(game.model || (game.model = {}));
    var model = game.model;
})(game || (game = {}));
var game;
(function (game) {
    (function (model) {
        var BehaviorDirectionVO = game.model.vo.BehaviorDirectionVO;

        var CharacterBehavior = (function () {
            function CharacterBehavior() {
            }
            CharacterBehavior.prototype.dispose = function () {
                this._aDirection = null;
            };

            CharacterBehavior.prototype.simulationSteps = function () {
                return this._aDirection.length;
            };

            CharacterBehavior.prototype.gameSteps = function () {
                return this._playBackCount;
            };

            CharacterBehavior.prototype.startSimulation = function () {
                this._simulationMode = true;
                this._aDirection = new Array();
            };

            CharacterBehavior.prototype.startGame = function () {
                this._simulationMode = false;
                this._playBack = -1;
                this._playBackCount = 0;
            };

            CharacterBehavior.prototype.getNextDirection = function () {
                if (this._simulationMode) {
                    var direction = new BehaviorDirectionVO();
                    direction.randomize(this._aDirection.length == 0);
                    this._aDirection.push(direction);
                    return direction;
                } else {
                    this._playBack++;
                    this._playBackCount++;

                    if (this._playBack > this._aDirection.length - 1)
                        this._playBack = this._aDirection.length - 1;

                    return this._aDirection[this._playBack];
                }

                return null;
            };
            return CharacterBehavior;
        })();
        model.CharacterBehavior = CharacterBehavior;
    })(game.model || (game.model = {}));
    var model = game.model;
})(game || (game = {}));
var game;
(function (game) {
    (function (model) {
        (function (enums) {
            var DirectionEnum = (function () {
                function DirectionEnum() {
                }
                DirectionEnum.DOWN = 0;
                DirectionEnum.LEFT = 1;
                DirectionEnum.RIGHT = 2;
                DirectionEnum.UP = 3;
                return DirectionEnum;
            })();
            enums.DirectionEnum = DirectionEnum;
        })(model.enums || (model.enums = {}));
        var enums = model.enums;
    })(game.model || (game.model = {}));
    var model = game.model;
})(game || (game = {}));
var game;
(function (game) {
    (function (screen) {
        (function (ingame) {
            var Treasure = (function (_super) {
                __extends(Treasure, _super);
                function Treasure() {
                    _super.call(this);

                    var image = PIXI.Sprite.fromFrame("treasure.png");
                    image.anchor = new PIXI.Point(0.5, 0.5);
                    this.addChild(image);
                }
                return Treasure;
            })(PIXI.DisplayObjectContainer);
            ingame.Treasure = Treasure;
        })(screen.ingame || (screen.ingame = {}));
        var ingame = screen.ingame;
    })(game.screen || (game.screen = {}));
    var screen = game.screen;
})(game || (game = {}));
var game;
(function (game) {
    (function (screen) {
        (function (ingame) {
            var SignalManager = util.signal.SignalManager;

            var Signals = game.signal.Signals;
            var Config = game.model.Config;
            var CharacterBehavior = game.model.CharacterBehavior;
            var DirectionEnum = game.model.enums.DirectionEnum;

            var SoundManager = util.audio.SoundManager;

            var Character = (function (_super) {
                __extends(Character, _super);
                function Character(num) {
                    var _this = this;
                    _super.call(this);
                    this.onStep = function () {
                        if (_this._foundExit)
                            return;

                        switch (_this._direction) {
                            case DirectionEnum.DOWN:
                                _this.position.y += _this._speed;
                                break;
                            case DirectionEnum.LEFT:
                                _this.position.x -= _this._speed;
                                break;
                            case DirectionEnum.RIGHT:
                                _this.position.x += _this._speed;
                                break;
                            case DirectionEnum.UP:
                                _this.position.y -= _this._speed;
                                break;
                        }

                        _this.checkPosition();
                        _this.checkBounds();

                        if (_this._foundExit)
                            SignalManager.invokeSignal(Signals.REACHED_EXIT, _this);
                    };

                    this.charID = num;
                    this.behavior = new CharacterBehavior();

                    Character.CHARACTER_FRAME = new PIXI.Rectangle(0, 0, 32, 32);

                    this._treasureIcon = PIXI.Sprite.fromFrame("exclamation.png");
                    this._treasureIcon.anchor = new PIXI.Point(0.5, 1);
                    this._treasureIcon.position = new PIXI.Point(0, -20);
                }
                Character.prototype.dispose = function () {
                    this.behavior.dispose();
                    this.behavior = null;

                    if (this._animation != null)
                        this._animation.stop();
                    this._animation = null;

                    this._walkLeft = null;
                    this._walkRight = null;
                    this._walkUp = null;
                    this._walkDown = null;

                    this._refLevel = null;
                };

                Character.prototype.setLevel = function (refLevel) {
                    this._refLevel = refLevel;
                };

                Character.prototype.init = function () {
                    this._speed = Config.DATA.characters.speed;
                    this._treasureIcon.visible = false;
                    this._exitInSight = false;
                    this._foundExit = false;

                    this.changeRandom();
                };

                Character.prototype.setCharacter = function (num) {
                    this.charSkinID = num;

                    log.Logger.log("char: " + this.charID + ", set skin: " + this.charSkinID);

                    this._walkDown = this.getAnimation(DirectionEnum.DOWN);
                    this._walkLeft = this.getAnimation(DirectionEnum.LEFT);
                    this._walkRight = this.getAnimation(DirectionEnum.RIGHT);
                    this._walkUp = this.getAnimation(DirectionEnum.UP);

                    this._animation = new PIXI.MovieClip(this._walkDown);
                    this._animation.anchor = new PIXI.Point(0.5, 0.5);
                    this.addChild(this._animation);

                    this._animation.loop = true;
                    this._animation.animationSpeed = Config.DATA.characters.animation_speed;

                    this._animation.play();

                    this.addChild(this._treasureIcon);
                };

                Character.prototype.changeDirection = function (direction) {
                    if (this._direction == direction)
                        return;

                    this._direction = direction;

                    if (this._animation != null) {
                        switch (direction) {
                            case DirectionEnum.DOWN:
                                this._animation.textures = this._walkDown;
                                break;
                            case DirectionEnum.LEFT:
                                this._animation.textures = this._walkLeft;
                                break;
                            case DirectionEnum.RIGHT:
                                this._animation.textures = this._walkRight;
                                break;
                            case DirectionEnum.UP:
                                this._animation.textures = this._walkUp;
                                break;
                        }
                    }
                };

                Character.prototype.aimExit = function () {
                    var dx = Math.round(this.distanceToExitX());
                    var dy = Math.round(this.distanceToExitY());

                    if (Math.abs(dx) < this._speed)
                        dx = 0;

                    if (dx > 0)
                        this.changeDirection(DirectionEnum.RIGHT);
                    else if (dx < 0)
                        this.changeDirection(DirectionEnum.LEFT);
                    else {
                        if (dy > 0)
                            this.changeDirection(DirectionEnum.DOWN);
                        else
                            this.changeDirection(DirectionEnum.UP);
                    }
                };

                Character.prototype.checkBounds = function () {
                    if (this.position.x < this._refLevel.rect.x) {
                        this.position.x = this._refLevel.rect.x;
                        this.changeDirection(DirectionEnum.RIGHT);
                    }
                    if (this.position.x > this._refLevel.rect.x + this._refLevel.rect.width) {
                        this.position.x = this._refLevel.rect.x + this._refLevel.rect.width;
                        this.changeDirection(DirectionEnum.LEFT);
                    }
                    if (this.position.y < this._refLevel.rect.y) {
                        this.position.y = this._refLevel.rect.y;
                        this.changeDirection(DirectionEnum.DOWN);
                    }
                    if (this.position.y > this._refLevel.rect.y + this._refLevel.rect.height) {
                        this.position.y = this._refLevel.rect.y + this._refLevel.rect.height;
                        this.changeDirection(DirectionEnum.UP);
                    }
                };

                Character.prototype.checkPosition = function () {
                    this._distanceToExit = this.distanceToExit();
                    this._foundExit = this._distanceToExit < 2;

                    var inSight = this._distanceToExit < Config.DATA.characters.view_distance;
                    if (this._animation != null && !this._exitInSight && inSight) {
                        SoundManager.playSound("found");
                    }
                    this._exitInSight = inSight;
                    this._treasureIcon.visible = this._exitInSight;

                    if (this._exitInSight) {
                        this.aimExit();
                    } else {
                        this._nextChangeDirection--;
                        if (this._nextChangeDirection == 0)
                            this.changeRandom();
                    }
                };

                Character.prototype.distanceToExit = function () {
                    var dx = this.distanceToExitX();
                    var dy = this.distanceToExitY();
                    return Math.sqrt(dx * dx + dy * dy);
                };

                Character.prototype.distanceToExitX = function () {
                    return this._refLevel.exit.x - this.position.x;
                };

                Character.prototype.distanceToExitY = function () {
                    return this._refLevel.exit.y - this.position.y;
                };

                Character.prototype.changeRandom = function () {
                    var nextBehavior = this.behavior.getNextDirection();
                    this.changeDirection(nextBehavior.direction);
                    this._nextChangeDirection = nextBehavior.stepDuration;
                };

                Character.prototype.getAnimation = function (direction) {
                    var animation = new Array();
                    var imgOffset = (this.charSkinID * 3) + direction * 12;
                    var imgIndex;

                    for (var i = 0, len = 3; i < len; i++) {
                        imgIndex = imgOffset + i + 1;
                        animation.push(PIXI.Texture.fromFrame("char-" + (imgIndex < 10 ? "0" + String(imgIndex) : String(imgIndex)) + ".png"));
                    }
                    return animation;
                };
                return Character;
            })(PIXI.DisplayObjectContainer);
            ingame.Character = Character;
        })(screen.ingame || (screen.ingame = {}));
        var ingame = screen.ingame;
    })(game.screen || (game.screen = {}));
    var screen = game.screen;
})(game || (game = {}));
var game;
(function (game) {
    (function (screen) {
        (function (ingame) {
            var CommonButton = util.ui.CommonButton;

            var CharacterChoiceButton = (function (_super) {
                __extends(CharacterChoiceButton, _super);
                function CharacterChoiceButton(charSkinID) {
                    this.skinID = charSkinID;

                    var imgIndex = (charSkinID * 3) + 1;
                    var textureName = "char-" + (imgIndex < 10 ? "0" + String(imgIndex) : String(imgIndex)) + ".png";
                    _super.call(this, textureName, textureName);

                    this.scale = new PIXI.Point(1.5, 1.5);
                }
                return CharacterChoiceButton;
            })(CommonButton);
            ingame.CharacterChoiceButton = CharacterChoiceButton;
        })(screen.ingame || (screen.ingame = {}));
        var ingame = screen.ingame;
    })(game.screen || (game.screen = {}));
    var screen = game.screen;
})(game || (game = {}));
var game;
(function (game) {
    (function (screen) {
        (function (ingame) {
            var CSS = util.ui.CSS;
            var Config = game.model.Config;
            var DisposeUtil = util.DisposeUtil;

            var CharacterChoice = (function (_super) {
                __extends(CharacterChoice, _super);
                function CharacterChoice() {
                    _super.call(this);
                    this._yOutside = -500;

                    this.tween = new TWEEN.Tween(this);
                    this.tween.easing(TWEEN.Easing.Cubic.Out);

                    this.position = new PIXI.Point(80, this._yOutside);

                    this.build();
                }
                CharacterChoice.prototype.build = function () {
                    var bg = PIXI.Sprite.fromFrame("panel_choice.png");
                    this.addChild(bg);

                    var font = CSS.getFont(CSS.FONT_Pusab, 40, "#ffffff");
                    font.wordWrap = true;
                    font.wordWrapWidth = 450;
                    CSS.addStroke(font, "#9d5a26", 8);

                    var title = new PIXI.Text("SELECT A HERO:", font);
                    title.anchor.x = 0.5;
                    title.position = new PIXI.Point(250, 230);
                    this.addChild(title);

                    var font = CSS.getFont(CSS.FONT_Pusab, 20, "#ffffff");
                    font.wordWrap = true;
                    font.wordWrapWidth = 350;
                    CSS.addStroke(font, "#9d5a26", 4);

                    var subtitle = new PIXI.Text(Config.DATA.texts.ingame, font);
                    subtitle.anchor.x = 0.5;
                    subtitle.position = new PIXI.Point(250, 290);
                    this.addChild(subtitle);

                    this._aButton = new Array();
                    for (var i = 0, len = Config.DATA.characters.number_of_characters; i < len; i++) {
                        var button = new ingame.CharacterChoiceButton(i);
                        button.buttonFamily = "char_choice";
                        button.position = new PIXI.Point(150 + i * 64, 380);
                        this.addChild(button);

                        this._aButton.push(button);
                    }

                    this._coin = PIXI.Sprite.fromFrame("coin.png");
                    this._coin.anchor = new PIXI.Point(0.5, 0.5);
                    this._coin.scale = new PIXI.Point(0.6, 0.6);
                    this._coin.visible = false;
                    this.addChild(this._coin);
                };

                CharacterChoice.prototype.dispose = function () {
                    for (var i = 0, len = Config.DATA.characters.number_of_characters; i < len; i++) {
                        this._aButton[i].dispose();
                    }
                    this._aButton = null;

                    DisposeUtil.dispose(this);

                    if (this.tween != null)
                        this.tween.stop();
                    this.tween = null;
                };

                CharacterChoice.prototype.selectChar = function (bt) {
                    this._coin.visible = true;
                    this._coin.position = new PIXI.Point(bt.position.x, bt.position.y + 50);
                };

                CharacterChoice.prototype.show = function () {
                    this.tween.stop();
                    this.tween.to({ y: 0 }, 1500);
                    this.tween.start();
                };

                CharacterChoice.prototype.hide = function () {
                    this.tween.stop();
                    this.tween.to({ y: this._yOutside }, 1500);
                    this.tween.start();
                };
                return CharacterChoice;
            })(PIXI.DisplayObjectContainer);
            ingame.CharacterChoice = CharacterChoice;
        })(screen.ingame || (screen.ingame = {}));
        var ingame = screen.ingame;
    })(game.screen || (game.screen = {}));
    var screen = game.screen;
})(game || (game = {}));
var game;
(function (game) {
    (function (screen) {
        var Logger = log.Logger;

        var CommonButton = util.ui.CommonButton;
        var Character = game.screen.ingame.Character;
        var Level = game.model.Level;
        var Treasure = game.screen.ingame.Treasure;
        var SignalManager = util.signal.SignalManager;
        var CoreSignals = util.signal.CoreSignals;
        var Config = game.model.Config;
        var Signals = game.signal.Signals;
        var CharacterChoice = game.screen.ingame.CharacterChoice;
        var GameVO = game.model.vo.GameVO;
        var ScreenEnum = game.model.enums.ScreenEnum;
        var SoundManager = util.audio.SoundManager;

        var InGameScreen = (function (_super) {
            __extends(InGameScreen, _super);
            function InGameScreen() {
                var _this = this;
                _super.call(this);
                this.onEnterFrameSimulation = function () {
                    if (_this._gameOver)
                        return;

                    Logger.log("onEnterFrameSimulation ");

                    for (var i = 0, len = 10000; i < len; i++) {
                        if (_this._gameOver)
                            break;
                        _this.updateCharacters();
                    }

                    if (_this._gameOver)
                        _this.endOfSimulation();
                };
                this.onButtonClick = function (buttonRef) {
                    if (buttonRef.buttonFamily == "char_choice") {
                        SignalManager.removeCallbackSignal(Signals.DEFAULT_BUTTON_CLICK, _this.onButtonClick);
                        Config.CURRENT_GAME.selectedSkinID = buttonRef.skinID;
                        _this._choice.selectChar(buttonRef);

                        Config.USER.useCoin();
                        _this._credits.update();
                        _this.startGame();
                    }

                    if (buttonRef.buttonFamily == "ingame_close") {
                        game.GameStage.goToScreen(ScreenEnum.MAIN);
                    }
                };
                this.onEnterFrame = function () {
                    if (_this._gameOver) {
                        _this.endOfGame();
                        return;
                    }

                    _this.updateCharacters();
                };
                this.delayedEndOfGame = function () {
                    game.GameStage.goToScreen(ScreenEnum.END_GAME);
                };
                this.onCharacterWin = function (char) {
                    Logger.log("a character has reached exit " + char.charID);
                    Config.CURRENT_GAME.predictedWinnerID = char.charID;

                    _this._gameOver = true;

                    if (_this._simulationMode) {
                        Logger.log("character simulation steps " + char.behavior.simulationSteps());
                    } else {
                        Logger.log("a character game steps " + char.behavior.gameSteps());
                    }
                };
                this.id = "ingame";
            }
            InGameScreen.prototype.onAdded = function () {
                this.defaultCursor = "pointer";

                var bgID = Math.round(Math.random() * 3);
                var bg = PIXI.Sprite.fromImage("./assets/images/bg" + String(bgID) + ".jpg");
                this.addChild(bg);

                this._treasure = new Treasure();
                this.addChild(this._treasure);

                this._aChar = new Array();
                for (var i = 0, len = Config.DATA.characters.number_of_characters; i < len; i++) {
                    var char = new Character(i);
                    this.addChild(char);

                    this._aChar.push(char);
                }

                this._choice = new CharacterChoice();
                this.addChild(this._choice);

                var title = new screen.Title();
                this.addChild(title);

                this._close = new CommonButton("close.png", "close.png");
                this._close.position = new PIXI.Point(600, 40);
                this._close.buttonFamily = "ingame_close";
                this.addChild(this._close);

                this._credits = new screen.Credits();
                this.addChild(this._credits);

                SoundManager.playGameBG("ingame_loop");
                this.reset();
            };

            InGameScreen.prototype.reset = function () {
                this._level = new Level(80, 110, 530, 530);
                this._treasure.position = this._level.exit;

                for (var i = 0, len = Config.DATA.characters.number_of_characters; i < len; i++) {
                    this._aChar[i].setLevel(this._level);
                }

                SignalManager.addCallbackSignal(Signals.REACHED_EXIT, this.onCharacterWin);

                Config.CURRENT_GAME = new GameVO();
                Config.CURRENT_GAME.setPredictiveMode(Config.DATA.predictive_win.mode, Config.DATA.predictive_win.random_win_rate);

                this.simulateGame();
            };

            InGameScreen.prototype.onRemoved = function () {
                SignalManager.removeCallbackSignal(Signals.DEFAULT_BUTTON_CLICK, this.onButtonClick);
                SignalManager.removeCallbackSignal(Signals.REACHED_EXIT, this.onCharacterWin);

                this._level = null;

                for (var i = 0, len = Config.DATA.characters.number_of_characters; i < len; i++) {
                    this._aChar[i].dispose();
                }
                this._aChar = null;

                this._treasure = null;

                this._choice.dispose();
                this._choice = null;

                _super.prototype.onRemoved.call(this);
            };

            InGameScreen.prototype.simulateGame = function () {
                this._gameOver = false;
                this._simulationMode = true;

                for (var i = 0, len = Config.DATA.characters.number_of_characters; i < len; i++) {
                    this._aChar[i].position = this._level.center.clone();
                    this._aChar[i].behavior.startSimulation();
                    this._aChar[i].init();
                }

                SignalManager.addCallbackSignal(CoreSignals.ON_ENTERFRAME, this.onEnterFrameSimulation);
            };

            InGameScreen.prototype.endOfSimulation = function () {
                SignalManager.removeCallbackSignal(CoreSignals.ON_ENTERFRAME, this.onEnterFrameSimulation);
                Logger.log("endOfSimulation ");

                this._choice.show();
                SignalManager.addCallbackSignal(Signals.DEFAULT_BUTTON_CLICK, this.onButtonClick);
            };

            InGameScreen.prototype.startGame = function () {
                Logger.log("startGame");
                this._gameOver = false;
                this._simulationMode = false;

                var aCharIndex = this.getCharsIndex();
                for (var i = 0, len = Config.DATA.characters.number_of_characters; i < len; i++) {
                    this._aChar[i].setCharacter(aCharIndex[i]);

                    this._aChar[i].position = this._level.center.clone();
                    this._aChar[i].behavior.startGame();
                    this._aChar[i].init();
                }
                this._choice.hide();
                SignalManager.addCallbackSignal(CoreSignals.ON_ENTERFRAME, this.onEnterFrame);
            };

            InGameScreen.prototype.getCharsIndex = function () {
                var result = new Array();
                var pointer;
                var skin;

                if (Config.CURRENT_GAME.playerShouldWin) {
                    pointer = Config.CURRENT_GAME.predictedWinnerID;
                    skin = Config.CURRENT_GAME.selectedSkinID;
                } else {
                    pointer = this.gapIndex(Config.CURRENT_GAME.predictedWinnerID + 1);
                    skin = Config.CURRENT_GAME.selectedSkinID;
                }

                for (var i = 0, len = Config.DATA.characters.number_of_characters; i < len; i++) {
                    result[pointer] = skin;

                    pointer = this.gapIndex(pointer + 1);
                    skin = this.gapIndex(skin + 1);
                }

                return result;
            };

            InGameScreen.prototype.gapIndex = function (value) {
                if (value >= Config.DATA.characters.number_of_characters)
                    return 0;
                return value;
            };

            InGameScreen.prototype.endOfGame = function () {
                SignalManager.removeCallbackSignal(Signals.REACHED_EXIT, this.onCharacterWin);
                SignalManager.removeCallbackSignal(CoreSignals.ON_ENTERFRAME, this.onEnterFrame);
                Logger.log("endOfGame ");

                setTimeout(this.delayedEndOfGame, 1500);
            };

            InGameScreen.prototype.updateCharacters = function () {
                for (var i = 0, len = Config.DATA.characters.number_of_characters; i < len; i++) {
                    this._aChar[i].onStep();
                }
            };
            return InGameScreen;
        })(game.AScreen);
        screen.InGameScreen = InGameScreen;
    })(game.screen || (game.screen = {}));
    var screen = game.screen;
})(game || (game = {}));
var game;
(function (game) {
    (function (screen) {
        var Logger = log.Logger;
        var CSS = util.ui.CSS;
        var Config = game.model.Config;
        var CommonButton = util.ui.CommonButton;
        var SignalManager = util.signal.SignalManager;
        var Signals = game.signal.Signals;
        var ScreenEnum = game.model.enums.ScreenEnum;
        var GameVO = game.model.vo.GameVO;
        var SoundManager = util.audio.SoundManager;

        var EndGameScreen = (function (_super) {
            __extends(EndGameScreen, _super);
            function EndGameScreen() {
                var _this = this;
                _super.call(this);
                this.onButtonClick = function (buttonRef) {
                    Logger.log("onButtonClick endgame");
                    if (buttonRef.buttonFamily == "end_button") {
                        SignalManager.removeCallbackSignal(Signals.DEFAULT_BUTTON_CLICK, _this.onButtonClick);
                        game.GameStage.goToScreen(ScreenEnum.MAIN);
                    }
                };
                this.id = "endgame";
            }
            EndGameScreen.prototype.onAdded = function () {
                if (Config.CURRENT_GAME == null) {
                    Config.CURRENT_GAME = new GameVO();
                    Config.CURRENT_GAME.playerShouldWin = false;
                }

                var info_txt;
                var bg_image;
                if (Config.CURRENT_GAME.playerShouldWin) {
                    info_txt = Config.DATA.texts.endScreen_win;
                    bg_image = "./assets/images/end_bg.jpg";
                } else {
                    info_txt = Config.DATA.texts.endScreen_loose;
                    bg_image = "./assets/images/end_bg_loose.jpg";
                }

                var bg = PIXI.Sprite.fromImage(bg_image);
                this.addChild(bg);

                var font = CSS.getFont(CSS.FONT_Pusab, 50, "#cd3937");
                font.wordWrap = true;
                font.wordWrapWidth = 522;

                var title = new screen.Title();
                this.addChild(title);

                var font = CSS.getFont(CSS.FONT_Pusab, 30, "#ffffff");
                CSS.addStroke(font, "#272e26", 8);
                font.wordWrap = true;
                font.wordWrapWidth = 522;

                var help = new PIXI.Text(info_txt, font);
                help.anchor.x = 0.5;
                help.position = new PIXI.Point(Math.round(Config.GAME_WIDTH / 2), 450);
                this.addChild(help);

                this._endBt = new CommonButton("button_out.png", "button_over.png");
                this._endBt.buttonFamily = "end_button";
                this._endBt.position = new PIXI.Point(Math.round(Config.GAME_WIDTH / 2), 700);
                this._endBt.setLabel("NEXT");
                this.addChild(this._endBt);

                this._credits = new screen.Credits();
                this.addChild(this._credits);

                if (Config.CURRENT_GAME.playerShouldWin) {
                    Config.USER.winCoins(5);
                    this._credits.update(true);
                }

                Logger.log("onAdded endgame");
                SignalManager.addCallbackSignal(Signals.DEFAULT_BUTTON_CLICK, this.onButtonClick);

                SoundManager.playGameBG("intro_loop");
                _super.prototype.onAdded.call(this);
            };

            EndGameScreen.prototype.onRemoved = function () {
                SignalManager.removeCallbackSignal(Signals.DEFAULT_BUTTON_CLICK, this.onButtonClick);

                this._endBt.dispose();
                this._endBt = null;

                _super.prototype.onRemoved.call(this);
            };
            return EndGameScreen;
        })(game.AScreen);
        screen.EndGameScreen = EndGameScreen;
    })(game.screen || (game.screen = {}));
    var screen = game.screen;
})(game || (game = {}));
var game;
(function (game) {
    var SignalManager = util.signal.SignalManager;

    var Logger = log.Logger;
    var CoreSignals = util.signal.CoreSignals;
    var ScreenEnum = game.model.enums.ScreenEnum;
    var LoadingScreen = game.screen.LoadingScreen;
    var MainScreen = game.screen.MainScreen;
    var InGameScreen = game.screen.InGameScreen;
    var EndGameScreen = game.screen.EndGameScreen;
    var Config = game.model.Config;

    var GameStage = (function () {
        function GameStage() {
        }
        GameStage.create = function (displaymode, scale, isMobile) {
            if (typeof scale === "undefined") { scale = false; }
            if (typeof isMobile === "undefined") { isMobile = false; }
            if (GameStage.renderer)
                return this;

            Config.IS_MOBILE = isMobile;

            GameStage.adjustRatio();

            var rendererOptions = {
                antialiasing: false,
                transparent: false,
                resolution: 1
            };

            Logger.log("init pixi mode " + displaymode);

            switch (displaymode) {
                case 0:
                    GameStage.renderer = PIXI.autoDetectRenderer(GameStage.width, GameStage.height, rendererOptions);
                    break;
                case 1:
                    GameStage.renderer = new PIXI.CanvasRenderer(GameStage.width, GameStage.height, rendererOptions);
                    break;
                case 2:
                    GameStage.renderer = new PIXI.WebGLRenderer(GameStage.width, GameStage.height, rendererOptions);
                    break;
            }

            document.getElementById('game-content').appendChild(GameStage.renderer.view);

            GameStage._stage = new PIXI.Stage(0xFFFFFF);
            console.log("stage created");

            if (scale) {
                GameStage._rescale();
                window.addEventListener('resize', GameStage._rescale, false);
            }

            requestAnimFrame(GameStage.loop);
            return this;
        };

        GameStage.adjustRatio = function () {
            GameStage.ratio = Math.min(window.innerWidth / Config.GAME_WIDTH, window.innerHeight / Config.GAME_HEIGHT);

            GameStage.width = Math.round(Config.GAME_WIDTH * GameStage.ratio);
            GameStage.height = Math.round(Config.GAME_HEIGHT * GameStage.ratio);
        };

        GameStage.loop = function () {
            requestAnimFrame(GameStage.loop);

            TWEEN.update();

            if (!GameStage.currentScreen || GameStage.currentScreen.isPaused()) {
                console.log("no render");
                return;
            }

            SignalManager.invokeSignal(CoreSignals.ON_ENTERFRAME);
            GameStage.renderer.render(GameStage._stage);
        };

        GameStage.goToScreen = function (id) {
            if (GameStage.currentScreen) {
                GameStage._stage.removeChild(GameStage.currentScreen);
                GameStage.currentScreen.onRemoved();
                GameStage.currentScreen = null;
            }

            switch (id) {
                case ScreenEnum.LOADING:
                    GameStage.currentScreen = new LoadingScreen();
                    break;
                case ScreenEnum.MAIN:
                    GameStage.currentScreen = new MainScreen();
                    break;
                case ScreenEnum.INGAME:
                    GameStage.currentScreen = new InGameScreen();
                    break;
                case ScreenEnum.END_GAME:
                    GameStage.currentScreen = new EndGameScreen();
                    break;
            }

            if (GameStage.currentScreen != null) {
                GameStage._stage.addChild(GameStage.currentScreen);
                GameStage.currentScreen.onAdded();
            }
            GameStage._rescale();

            return true;
        };
        GameStage._rescale = function () {
            GameStage.adjustRatio();

            GameStage.renderer.resize(GameStage.width, GameStage.height);

            if (GameStage.currentScreen != undefined) {
                GameStage.currentScreen.rescale(GameStage.ratio);
            }
        };
        return GameStage;
    })();
    game.GameStage = GameStage;
})(game || (game = {}));
var GameStage = game.GameStage;
var ScreenEnum = game.model.enums.ScreenEnum;
var Config = game.model.Config;

function init(userObj, debug) {
    var isMobile = false;

    if (userObj.device.type == "Mobile" || userObj.device.type == "Tablet")
        isMobile = true;
    else if (userObj.device.type == "Desktop" && userObj.os.family == "Android") {
        isMobile = true;
    }

    log.Logger.init(debug);
    log.Logger.log("INIT ");

    util.signal.SignalManager.init();
    util.Clock.init();

    GameStage.create(0, true, isMobile);
    GameStage.goToScreen(ScreenEnum.LOADING);
}
//# sourceMappingURL=game.dev.js.map
