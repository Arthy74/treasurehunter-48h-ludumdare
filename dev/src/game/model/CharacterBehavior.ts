///<reference path="./vo/BehaviorDirectionVO.ts" />

module game.model {

    import BehaviorDirectionVO = game.model.vo.BehaviorDirectionVO;

    export class CharacterBehavior {

        private _aDirection:Array<BehaviorDirectionVO>;
        private _simulationMode:boolean;

        private _playBack:number;
        private _playBackCount:number;

        constructor() {
        }

        public dispose()
        {
            this._aDirection = null;
        }

        public simulationSteps():number
        {
            return this._aDirection.length;
        }

        public gameSteps():number
        {
            return this._playBackCount;
        }

        public startSimulation()
        {
            this._simulationMode = true;
            this._aDirection = new Array<BehaviorDirectionVO>();
        }

        public startGame()
        {
            this._simulationMode = false;
            this._playBack = -1;
            this._playBackCount = 0;
        }

        public getNextDirection():BehaviorDirectionVO
        {
            if (this._simulationMode)
            {
                var direction:BehaviorDirectionVO = new BehaviorDirectionVO();
                direction.randomize(this._aDirection.length==0);
                this._aDirection.push(direction);
                return direction;
            }
            else
            {
                this._playBack++;
                this._playBackCount++;

                if (this._playBack > this._aDirection.length - 1)
                    this._playBack = this._aDirection.length - 1;

                return this._aDirection[this._playBack];
            }

            return null;
        }
    }
}