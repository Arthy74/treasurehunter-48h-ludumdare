module game.model.vo {
    export class GameVO {

        public selectedSkinID:number;
        public predictedWinnerID:number;

        public playerShouldWin:boolean;

        constructor() {
        }

        public setPredictiveMode(num:number, randomWinRate:number)
        {
            switch (num)
            {
                case 0:
                    // random win
                    this.playerShouldWin = (Math.random() < randomWinRate);
                    break;
                case 1:
                    // always win
                    this.playerShouldWin = true;
                    break;
                case 2:
                    // always loose
                    this.playerShouldWin = false;
                    break;
            }
        }
    }
}