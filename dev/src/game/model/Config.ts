///<reference path="../../util/io/IORequest.ts" />
///<reference path="../../util/signal/SignalManager.ts" />
///<reference path="../../util/signal/CoreSignals.ts" />

///<reference path="./vo/GameVO.ts" />
///<reference path="./UserModel.ts" />

module game.model {

    import IORequest = util.io.IORequest;
    import SignalManager = util.signal.SignalManager;
    import CoreSignals = util.signal.CoreSignals;
    import GameVO = game.model.vo.GameVO;

    export class Config {

        public static DATA:any;
        public static IS_MOBILE:boolean;

        public static GAME_WIDTH:number = 640;
        public static GAME_HEIGHT:number = 960;

        public static USER:UserModel;
        public static CURRENT_GAME:GameVO;

        public static init():void {
            Config.USER = new UserModel();

            var timestamp = new Date().getTime();
            var req:IORequest = new IORequest("assets/config.json?t=" + timestamp);
            req.sendRequest(Config.onGetConfigSuccess, Config.onGetConfigFail);
        }

        public static onGetConfigSuccess = (json:any)=> {
            log.Logger.log("JSON ", json);

            Config.DATA = json;
            Config.IS_MOBILE = false;

            Config.USER.init(json.user);

            SignalManager.invokeSignal(CoreSignals.CONFIG_LOADED);
        }

        public static onGetConfigFail = (data:string)=> {
            log.Logger.log("error loading config ", data);
        }
    }
}