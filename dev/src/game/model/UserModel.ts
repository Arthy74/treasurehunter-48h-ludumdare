///<reference path="./vo/UserVO.ts" />

module game.model {
    import UserVO = game.model.vo.UserVO;

    export class UserModel {

        private _user:UserVO;

        constructor() {
        }

        public init(data:any)
        {
            this._user = new UserVO();
            this._user.name = data.name;
            this._user.credits = data.credits;
        }

        public winCoins(num:number)
        {
            this._user.credits += num;
        }

        public noCredits():boolean
        {
            return (this._user.credits == 0);
        }

        public getCredits():number
        {
            return this._user.credits;
        }

        public useCoin()
        {
            this._user.credits--;
        }
    }
}