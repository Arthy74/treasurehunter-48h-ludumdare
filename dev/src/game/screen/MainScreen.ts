///<reference path="../../../lib/PIXI.d.ts" />
///<reference path="../../../lib/Tween.js.d.ts" />
///<reference path="../../util/Log.ts" />
///<reference path="../AScreen.ts" />
///<reference path="../GameStage.ts"/>
///<reference path="../../util/ui/CSS.ts"/>
///<reference path="../../util/ui/CommonButton.ts"/>
///<reference path="../../util/signal/SignalManager.ts"/>
///<reference path="../../util/signal/CoreSignals.ts"/>
///<reference path="../signal/Signals.ts"/>
///<reference path="../model/enums/ScreenEnum.ts"/>
///<reference path="./common/Title.ts"/>
///<reference path="./common/Credits.ts"/>
///<reference path="../model/Config.ts"/>
///<reference path="../../util/audio/SoundManager.ts"/>

module game.screen {
    // Class
    import Logger = log.Logger;
    import CSS = util.ui.CSS;
    import CommonButton = util.ui.CommonButton;
    import Signals = game.signal.Signals;
    import SignalManager = util.signal.SignalManager;
    import CoreSignals = util.signal.CoreSignals;
    import ScreenEnum = game.model.enums.ScreenEnum;
    import Config = game.model.Config;
    import SoundManager = util.audio.SoundManager;

    export class MainScreen extends AScreen {

        private _startBt:CommonButton;

        constructor() {
            super();
            this.id = "main";
        }

        public onRemoved()
        {
            SignalManager.removeCallbackSignal(Signals.DEFAULT_BUTTON_CLICK, this.onButtonClick);

            this._startBt.dispose();
            this._startBt = null;

            super.onRemoved();
        }

        public onAdded()
        {
            var bg:PIXI.Sprite = PIXI.Sprite.fromImage("./assets/images/intro_bg.jpg");
            this.addChild(bg);

            var title:Title = new Title();
            this.addChild(title);

            var treasure:PIXI.Sprite = PIXI.Sprite.fromFrame("Treasure_big.png");
            treasure.anchor = new PIXI.Point(0.5, 0.5);
            treasure.position = new PIXI.Point(Math.round(Config.GAME_WIDTH/2), 320);
            this.addChild(treasure);

            var font:any = CSS.getFont(CSS.FONT_Pusab, 30, "#ffffff");
            CSS.addStroke(font, "#272e26", 8);
            font.wordWrap = true;
            font.wordWrapWidth = 522;

            var help:PIXI.Text = new PIXI.Text(Config.DATA.texts.help, font);
            help.anchor.x = 0.5;
            help.position = new PIXI.Point(Math.round(Config.GAME_WIDTH/2), 450);
            this.addChild(help);

            this._startBt = new CommonButton("button_out.png", "button_over.png");
            this._startBt.buttonFamily = "start_bt";
            this._startBt.position = new PIXI.Point(Math.round(Config.GAME_WIDTH/2), 700);
            this._startBt.setLabel("START");
            this.addChild(this._startBt);

            var credits:Credits = new Credits();
            this.addChild(credits);

            if (Config.USER.noCredits())
            {
                this._startBt.setActive(false);
                this._startBt.alpha = 0.4;

                var font:any = CSS.getFont(CSS.FONT_Pusab, 25, "#ffffff");
                CSS.addStroke(font, "#272e26", 8);
                font.wordWrap = true;
                font.wordWrapWidth = 522;

                var noCreditsTxt:PIXI.Text = new PIXI.Text(Config.DATA.texts.no_credits, font);
                noCreditsTxt.anchor.x = 0.5;
                noCreditsTxt.position = new PIXI.Point(Math.round(Config.GAME_WIDTH/2), 780);
                this.addChild(noCreditsTxt);
            }
            else
            {
                this._startBt.setActive(true);
                SignalManager.addCallbackSignal(Signals.DEFAULT_BUTTON_CLICK, this.onButtonClick);
            }

            SoundManager.playGameBG("intro_loop");
            super.onAdded();
        }

        private onButtonClick=(buttonRef)=>
        {
            if (buttonRef.buttonFamily == "start_bt") {
                GameStage.goToScreen(ScreenEnum.INGAME);
            }
        }
    }
}
