///<reference path="../../../../lib/PIXI.d.ts" />
///<reference path="../../../../lib/tween.js.d.ts" />

///<reference path="../../../util/signal/SignalManager.ts"/>
///<reference path="../../../util/signal/CoreSignals.ts"/>
///<reference path="../../signal/Signals.ts"/>
///<reference path="../../../util/ui/CSS.ts"/>
///<reference path="../../model/Config.ts"/>
///<reference path="./CharacterChoiceButton.ts"/>
///<reference path="../../../util/DisposeUtil.ts"/>

module game.screen.ingame {

    import CSS = util.ui.CSS;
    import Config = game.model.Config;
    import DisposeUtil = util.DisposeUtil;

    export class CharacterChoice extends PIXI.DisplayObjectContainer {

        public tween:TWEEN.Tween;

        private _yOutside:number = -500;
        private _coin:PIXI.Sprite;
        private _aButton:Array<CharacterChoiceButton>;

        constructor() {
            super();

            this.tween = new TWEEN.Tween(this);
            this.tween.easing(TWEEN.Easing.Cubic.Out);

            this.position = new PIXI.Point(80, this._yOutside);

            this.build();
        }

        public build() {
            var bg:PIXI.Sprite = PIXI.Sprite.fromFrame("panel_choice.png");
            this.addChild(bg);

            // TITLE
            var font:any = CSS.getFont(CSS.FONT_Pusab, 40, "#ffffff");
            font.wordWrap = true;
            font.wordWrapWidth = 450;
            CSS.addStroke(font, "#9d5a26", 8);

            var title:PIXI.Text = new PIXI.Text("SELECT A HERO:", font);
            title.anchor.x = 0.5;
            title.position = new PIXI.Point(250, 230);
            this.addChild(title);

            // SUBTITLE
            var font:any = CSS.getFont(CSS.FONT_Pusab, 20, "#ffffff");
            font.wordWrap = true;
            font.wordWrapWidth = 350;
            CSS.addStroke(font, "#9d5a26", 4);

            var subtitle:PIXI.Text = new PIXI.Text(Config.DATA.texts.ingame, font);
            subtitle.anchor.x = 0.5;
            subtitle.position = new PIXI.Point(250, 290);
            this.addChild(subtitle);

            this._aButton = new Array<CharacterChoiceButton>();
            for (var i = 0, len = Config.DATA.characters.number_of_characters; i < len; i++) {
                var button = new CharacterChoiceButton(i);
                button.buttonFamily = "char_choice";
                button.position = new PIXI.Point(150 + i* 64, 380);
                this.addChild(button);

                this._aButton.push(button);
            }

            this._coin = PIXI.Sprite.fromFrame("coin.png");
            this._coin.anchor = new PIXI.Point(0.5, 0.5);
            this._coin.scale = new PIXI.Point(0.6, 0.6);
            this._coin.visible = false;
            this.addChild(this._coin);
        }

        public dispose() {
            for (var i = 0, len = Config.DATA.characters.number_of_characters; i < len; i++) {
                this._aButton[i].dispose();
            }
            this._aButton = null;

            DisposeUtil.dispose(this);

            if (this.tween != null)
                this.tween.stop();
            this.tween = null;
        }

        public selectChar(bt:CharacterChoiceButton) {
            this._coin.visible = true;
            this._coin.position = new PIXI.Point(bt.position.x, bt.position.y + 50);
        }

        public show() {
            this.tween.stop();
            this.tween.to({y:0}, 1500);
            this.tween.start();
        }

        public hide() {
            this.tween.stop();
            this.tween.to({y:this._yOutside}, 1500);
            this.tween.start();
        }
    }
}