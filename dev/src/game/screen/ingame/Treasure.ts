///<reference path="../../../../lib/PIXI.d.ts" />

module game.screen.ingame {

    export class Treasure extends PIXI.DisplayObjectContainer {

        constructor() {
            super();

            var image:PIXI.Sprite = PIXI.Sprite.fromFrame("treasure.png");
            image.anchor = new PIXI.Point(0.5, 0.5);
            this.addChild(image);
        }
    }
}
