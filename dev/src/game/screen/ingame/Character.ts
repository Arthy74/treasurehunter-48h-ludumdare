///<reference path="../../../../lib/PIXI.d.ts" />

///<reference path="../../../util/signal/SignalManager.ts"/>
///<reference path="../../../util/signal/CoreSignals.ts"/>
///<reference path="../../signal/Signals.ts"/>
///<reference path="../../model/Level.ts"/>
///<reference path="../../model/Config.ts"/>
///<reference path="../../model/CharacterBehavior.ts"/>
///<reference path="../../model/vo/BehaviorDirectionVO.ts"/>
///<reference path="../../model/enums/DirectionEnum.ts"/>
///<reference path="./Treasure.ts"/>

module game.screen.ingame {

    import SignalManager = util.signal.SignalManager;
    import CoreSignals = util.signal.CoreSignals;
    import Level = game.model.Level;
    import Signals = game.signal.Signals;
    import Config = game.model.Config;
    import CharacterBehavior = game.model.CharacterBehavior;
    import DirectionEnum = game.model.enums.DirectionEnum;
    import BehaviorDirectionVO = game.model.vo.BehaviorDirectionVO;
    import SoundManager = util.audio.SoundManager;

    export class Character extends PIXI.DisplayObjectContainer {

        public static CHARACTER_FRAME:PIXI.Rectangle;
        public static CHARACTERS_TEXTURE:PIXI.Texture;

        public charID:number;
        public charSkinID:number;
        public behavior:CharacterBehavior;

        private _animation:PIXI.MovieClip;
        private _walkLeft : Array<PIXI.Texture>;
        private _walkRight : Array<PIXI.Texture>;
        private _walkUp : Array<PIXI.Texture>;
        private _walkDown : Array<PIXI.Texture>;

        private _direction : number;
        private _speed : number;

        private _refLevel:Level;
        private _nextChangeDirection:number;
        private _exitInSight:boolean;
        private _distanceToExit:number;
        private _foundExit:boolean;

        private _treasureIcon:PIXI.Sprite;

        constructor(num:number) {
            super();

            this.charID = num;
            this.behavior = new CharacterBehavior();

            Character.CHARACTER_FRAME = new PIXI.Rectangle(0, 0, 32, 32);

            this._treasureIcon = PIXI.Sprite.fromFrame("exclamation.png");
            this._treasureIcon.anchor = new PIXI.Point(0.5, 1);
            this._treasureIcon.position = new PIXI.Point(0, -20);
        }

        public dispose()
        {
            this.behavior.dispose();
            this.behavior = null;

            if (this._animation != null)
                this._animation.stop();
            this._animation = null;

            this._walkLeft = null;
            this._walkRight = null;
            this._walkUp = null;
            this._walkDown = null;

            this._refLevel = null;
        }

        public setLevel(refLevel:Level)
        {
            this._refLevel = refLevel;
        }

        public init()
        {
            this._speed = Config.DATA.characters.speed;
            this._treasureIcon.visible = false;
            this._exitInSight = false;
            this._foundExit = false;

            this.changeRandom();
        }

        public setCharacter(num:number) {
            this.charSkinID = num;

            log.Logger.log("char: " + this.charID + ", set skin: " + this.charSkinID);

            this._walkDown = this.getAnimation(DirectionEnum.DOWN);
            this._walkLeft = this.getAnimation(DirectionEnum.LEFT);
            this._walkRight = this.getAnimation(DirectionEnum.RIGHT);
            this._walkUp = this.getAnimation(DirectionEnum.UP);

            this._animation = new PIXI.MovieClip(this._walkDown);
            this._animation.anchor = new PIXI.Point(0.5, 0.5);
            this.addChild(this._animation);

            this._animation.loop = true;
            this._animation.animationSpeed = Config.DATA.characters.animation_speed;

            this._animation.play();

            this.addChild(this._treasureIcon);
        }

        public changeDirection(direction:number):void{
            if (this._direction == direction)
                return;

            this._direction = direction;

            if (this._animation != null) {
                switch (direction) {
                    case DirectionEnum.DOWN:
                        this._animation.textures = this._walkDown;
                        break;
                    case DirectionEnum.LEFT:
                        this._animation.textures = this._walkLeft;
                        break;
                    case DirectionEnum.RIGHT:
                        this._animation.textures = this._walkRight;
                        break;
                    case DirectionEnum.UP:
                        this._animation.textures = this._walkUp;
                        break;
                }
            }
        }

        public onStep=()=> {
            if (this._foundExit)
                return;

            switch (this._direction) {
                case DirectionEnum.DOWN:
                    this.position.y += this._speed;
                    break;
                case DirectionEnum.LEFT:
                    this.position.x -= this._speed;
                    break;
                case DirectionEnum.RIGHT:
                    this.position.x += this._speed;
                    break;
                case DirectionEnum.UP:
                    this.position.y -= this._speed;
                    break;
            }

            this.checkPosition();
            this.checkBounds();

            if (this._foundExit)
                SignalManager.invokeSignal(Signals.REACHED_EXIT, this);
        }

        private aimExit()
        {
            var dx:number = Math.round(this.distanceToExitX());
            var dy:number = Math.round(this.distanceToExitY());

            // move horiz first
            if (Math.abs(dx)<this._speed)
                dx = 0;

            if (dx>0) this.changeDirection(DirectionEnum.RIGHT);
            else if (dx<0)this.changeDirection(DirectionEnum.LEFT);
            else
            {
                // move vert
                if (dy>0) this.changeDirection(DirectionEnum.DOWN);
                else this.changeDirection(DirectionEnum.UP);
            }
        }

        private checkBounds()
        {
            if (this.position.x < this._refLevel.rect.x)
            {
                this.position.x = this._refLevel.rect.x;
                this.changeDirection(DirectionEnum.RIGHT)
            }
            if (this.position.x > this._refLevel.rect.x + this._refLevel.rect.width)
            {
                this.position.x = this._refLevel.rect.x + this._refLevel.rect.width;
                this.changeDirection(DirectionEnum.LEFT)
            }
            if (this.position.y < this._refLevel.rect.y)
            {
                this.position.y = this._refLevel.rect.y;
                this.changeDirection(DirectionEnum.DOWN)
            }
            if (this.position.y > this._refLevel.rect.y + this._refLevel.rect.height)
            {
                this.position.y = this._refLevel.rect.y + this._refLevel.rect.height;
                this.changeDirection(DirectionEnum.UP)
            }
        }

        private checkPosition()
        {
            this._distanceToExit = this.distanceToExit();
            this._foundExit = this._distanceToExit<2;

            var inSight = this._distanceToExit < Config.DATA.characters.view_distance;
            if (this._animation != null && !this._exitInSight && inSight)
            {
                SoundManager.playSound("found");
            }
            this._exitInSight = inSight;
            this._treasureIcon.visible = this._exitInSight;

            if (this._exitInSight)
            {
                this.aimExit();
            }
            else
            {
                this._nextChangeDirection--;
                if (this._nextChangeDirection == 0)
                    this.changeRandom();
            }
        }

        private distanceToExit():number
        {
            var dx:number = this.distanceToExitX();
            var dy:number = this.distanceToExitY();
            return Math.sqrt(dx*dx + dy*dy);
        }

        private distanceToExitX():number
        {
            return this._refLevel.exit.x - this.position.x;
        }

        private distanceToExitY():number
        {
            return this._refLevel.exit.y - this.position.y;
        }

        public changeRandom()
        {
            var nextBehavior:BehaviorDirectionVO = this.behavior.getNextDirection();
            this.changeDirection(nextBehavior.direction);
            this._nextChangeDirection = nextBehavior.stepDuration;
        }

        private getAnimation(direction:number):Array<PIXI.Texture>
        {
            var animation:Array<PIXI.Texture> = new Array<PIXI.Texture>();
            var imgOffset:number = (this.charSkinID * 3) + direction * 12;
            var imgIndex:number;

            for (var i = 0, len = 3; i < len; i++) {
                imgIndex = imgOffset + i + 1;
                animation.push(PIXI.Texture.fromFrame("char-" + (imgIndex <10 ? "0" + String(imgIndex) : String(imgIndex)) + ".png"));
            }
            return animation;
        }
    }
}