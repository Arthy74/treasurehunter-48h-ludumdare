///<reference path="../../../lib/PIXI.d.ts" />
///<reference path="../../../lib/Tween.js.d.ts" />
///<reference path="../../util/Log.ts" />
///<reference path="../AScreen.ts" />
///<reference path="../GameStage.ts"/>
///<reference path="../../util/ui/CSS.ts"/>
///<reference path="../../util/ui/CommonButton.ts"/>
///<reference path="./ingame/Character.ts"/>
///<reference path="./ingame/CharacterChoice.ts"/>
///<reference path="./ingame/Treasure.ts"/>
///<reference path="./common/Credits.ts"/>
///<reference path="../model/Level.ts"/>
///<reference path="../model/Config.ts"/>
///<reference path="../model/vo/GameVO.ts"/>
///<reference path="../../util/signal/SignalManager.ts"/>
///<reference path="../../util/signal/CoreSignals.ts"/>
///<reference path="../model/enums/ScreenEnum.ts"/>

module game.screen {
    // Class
    import Logger = log.Logger;
    import CSS = util.ui.CSS;
    import CommonButton = util.ui.CommonButton;
    import Character = game.screen.ingame.Character;
    import Level = game.model.Level;
    import Treasure = game.screen.ingame.Treasure;
    import SignalManager = util.signal.SignalManager;
    import CoreSignals = util.signal.CoreSignals;
    import Config = game.model.Config;
    import Signals = game.signal.Signals;
    import CharacterChoice = game.screen.ingame.CharacterChoice;
    import GameVO = game.model.vo.GameVO;
    import ScreenEnum = game.model.enums.ScreenEnum;
    import SoundManager = util.audio.SoundManager;

    export class InGameScreen extends AScreen {

        private _level:Level;

        private _aChar:Array<Character>;
        private _treasure:Treasure;
        private _gameOver:boolean;
        private _simulationMode:boolean;

        private _choice:CharacterChoice;
        private _close:CommonButton;
        private _credits:Credits;

        constructor() {
            super();
            this.id = "ingame";
        }

        public onAdded()
        {
            this.defaultCursor = "pointer";

            var bgID:number = Math.round(Math.random()*3);
            var bg:PIXI.Sprite = PIXI.Sprite.fromImage("./assets/images/bg" + String(bgID) + ".jpg");
            this.addChild(bg);

            this._treasure = new Treasure();
            this.addChild(this._treasure);

            this._aChar = new Array<Character>();
            for (var i = 0, len = Config.DATA.characters.number_of_characters; i < len; i++) {
                var char = new Character(i);
                this.addChild(char);

                this._aChar.push(char);
            }

            // interface
            this._choice = new CharacterChoice();
            this.addChild(this._choice);

            var title:Title = new Title();
            this.addChild(title);

            this._close = new CommonButton("close.png", "close.png");
            this._close.position = new PIXI.Point(600, 40);
            this._close.buttonFamily = "ingame_close";
            this.addChild(this._close);

            this._credits = new Credits();
            this.addChild(this._credits);

            SoundManager.playGameBG("ingame_loop");
            this.reset();
        }

        private reset()
        {
            this._level = new Level(80, 110, 530, 530);
            this._treasure.position = this._level.exit;

            for (var i = 0, len = Config.DATA.characters.number_of_characters; i < len; i++) {
                this._aChar[i].setLevel(this._level);
            }

            SignalManager.addCallbackSignal(Signals.REACHED_EXIT, this.onCharacterWin);

            Config.CURRENT_GAME = new GameVO();
            Config.CURRENT_GAME.setPredictiveMode(Config.DATA.predictive_win.mode, Config.DATA.predictive_win.random_win_rate);

            this.simulateGame();
        }

        public onRemoved()
        {
            SignalManager.removeCallbackSignal(Signals.DEFAULT_BUTTON_CLICK, this.onButtonClick);
            SignalManager.removeCallbackSignal(Signals.REACHED_EXIT, this.onCharacterWin);

            this._level = null;

            for (var i = 0, len = Config.DATA.characters.number_of_characters; i < len; i++) {
                this._aChar[i].dispose();
            }
            this._aChar = null;

            this._treasure = null;

            this._choice.dispose();
            this._choice = null;

            super.onRemoved();
        }

        // Phase 1: simulation
        private simulateGame()
        {
            this._gameOver = false;
            this._simulationMode = true;

            for (var i = 0, len = Config.DATA.characters.number_of_characters; i < len; i++) {
                this._aChar[i].position = this._level.center.clone();
                this._aChar[i].behavior.startSimulation();
                this._aChar[i].init();
            }

            SignalManager.addCallbackSignal(CoreSignals.ON_ENTERFRAME, this.onEnterFrameSimulation);
        }

        public onEnterFrameSimulation=()=> {
            if (this._gameOver)
                return;

            Logger.log("onEnterFrameSimulation ");

            for (var i = 0, len = 10000; i < len; i++) {
                if (this._gameOver) break;
                this.updateCharacters();
            }

            if (this._gameOver)
                this.endOfSimulation();
        }

        private endOfSimulation()
        {
            SignalManager.removeCallbackSignal(CoreSignals.ON_ENTERFRAME, this.onEnterFrameSimulation);
            Logger.log("endOfSimulation ");

            this._choice.show();
            SignalManager.addCallbackSignal(Signals.DEFAULT_BUTTON_CLICK, this.onButtonClick);
        }

        // Phase 2: play game
        private startGame()
        {
            Logger.log("startGame");
            this._gameOver = false;
            this._simulationMode = false;

            var aCharIndex:Array<number> = this.getCharsIndex();
            for (var i = 0, len = Config.DATA.characters.number_of_characters; i < len; i++) {

                // assign players
                this._aChar[i].setCharacter(aCharIndex[i]);

                this._aChar[i].position = this._level.center.clone();
                this._aChar[i].behavior.startGame();
                this._aChar[i].init();
            }
            this._choice.hide();
            SignalManager.addCallbackSignal(CoreSignals.ON_ENTERFRAME, this.onEnterFrame);
        }

        private getCharsIndex():Array<number>
        {
            var result:Array<number> = new Array<number>();
            var pointer:number;
            var skin:number;

            if (Config.CURRENT_GAME.playerShouldWin){
                pointer = Config.CURRENT_GAME.predictedWinnerID;
                skin = Config.CURRENT_GAME.selectedSkinID;
            }
            else
            {
                pointer = this.gapIndex(Config.CURRENT_GAME.predictedWinnerID + 1);
                skin = Config.CURRENT_GAME.selectedSkinID;
            }

            for (var i = 0, len = Config.DATA.characters.number_of_characters; i < len; i++) {
                result[pointer] = skin;

                pointer = this.gapIndex(pointer + 1);
                skin = this.gapIndex(skin + 1);
            }

            return result;
        }

        private gapIndex(value:number):number
        {
            if (value >= Config.DATA.characters.number_of_characters) return 0;
            return value;
        }

        public onButtonClick=(buttonRef:any)=> {
            if (buttonRef.buttonFamily == "char_choice") {
                SignalManager.removeCallbackSignal(Signals.DEFAULT_BUTTON_CLICK, this.onButtonClick);
                Config.CURRENT_GAME.selectedSkinID = buttonRef.skinID;
                this._choice.selectChar(buttonRef);

                Config.USER.useCoin();
                this._credits.update();
                this.startGame();
            }

            if (buttonRef.buttonFamily == "ingame_close") {
                GameStage.goToScreen(ScreenEnum.MAIN);
            }
        }

        public onEnterFrame=()=> {
            if (this._gameOver)
            {
                this.endOfGame();
                return;
            }

            this.updateCharacters();
        }

        private endOfGame()
        {
            SignalManager.removeCallbackSignal(Signals.REACHED_EXIT, this.onCharacterWin);
            SignalManager.removeCallbackSignal(CoreSignals.ON_ENTERFRAME, this.onEnterFrame);
            Logger.log("endOfGame ");

            setTimeout(this.delayedEndOfGame, 1500);
        }

        private delayedEndOfGame=()=>
        {
            GameStage.goToScreen(ScreenEnum.END_GAME);
        }

        private updateCharacters(){
            for (var i = 0, len = Config.DATA.characters.number_of_characters; i < len; i++) {
                this._aChar[i].onStep();
            }
        }

        public onCharacterWin=(char:Character)=> {
            Logger.log("a character has reached exit " + char.charID);
            Config.CURRENT_GAME.predictedWinnerID = char.charID;

            this._gameOver = true;

            if (this._simulationMode)
            {
                Logger.log("character simulation steps " + char.behavior.simulationSteps());
            }
            else
            {
                Logger.log("a character game steps " + char.behavior.gameSteps());
            }
        }
    }
}
