///<reference path="../../../lib/PIXI.d.ts" />
///<reference path="../../../lib/Tween.js.d.ts" />
///<reference path="../../util/Log.ts" />
///<reference path="../AScreen.ts" />
///<reference path="../GameStage.ts"/>
///<reference path="../model/Config.ts"/>
///<reference path="../../util/ui/CSS.ts"/>
///<reference path="../../util/signal/SignalManager.ts"/>
///<reference path="../../util/signal/CoreSignals.ts"/>
///<reference path="../../util/audio/SoundManager.ts"/>
///<reference path="../../util/ui/AssetsManager.ts"/>
///<reference path="../model/enums/ScreenEnum.ts"/>

module game.screen {
    // Class
    import Logger = log.Logger;
    import CSS = util.ui.CSS;
    import Config = game.model.Config;
    import SignalManager = util.signal.SignalManager;
    import SoundManager = util.audio.SoundManager;
    import SoundSignals = util.audio.signal.SoundSignals;
    import CoreSignals = util.signal.CoreSignals;
    import AssetsManager = util.ui.AssetsManager;
    import ScreenEnum = game.model.enums.ScreenEnum;

    export class LoadingScreen extends AScreen {

        private _progress:PIXI.Text;

        constructor() {
            super();
            this.id = "loading";
        }

        public onAdded()
        {
            var font:any = CSS.getFont(CSS.FONT_Pusab, 50, "#cd3937");
            font.wordWrap = true;
            font.wordWrapWidth = 522;

            var title:PIXI.Text = new PIXI.Text("Loading", font);
            title.anchor.x = 0.5;
            title.position = new PIXI.Point(Math.round(Config.GAME_WIDTH/2), 200);
            this.addChild(title);

            this._progress = new PIXI.Text("", font);
            this._progress.anchor.x = 0.5;
            this._progress.position = new PIXI.Point(Math.round(Config.GAME_WIDTH/2), 250);
            this.addChild(this._progress);

            SignalManager.addCallbackSignal(CoreSignals.CONFIG_LOADED, this.onConfigLoaded);
            Config.init();
        }

        public onRemoved()
        {
            SignalManager.removeCallbackSignal(CoreSignals.CONFIG_LOADED, this.onConfigLoaded);
            super.onRemoved();

            this._progress = null;
        }

        private onConfigLoaded=()=>
        {
            Logger.log("onConfigLoaded ");

            SignalManager.removeCallbackSignal(CoreSignals.CONFIG_LOADED, this.onConfigLoaded);

            SignalManager.addCallbackSignal(CoreSignals.ASSETS_LOADED, this.onAssetsLoaded);
            SignalManager.addCallbackSignal(CoreSignals.ASSETS_PROGRESS, this.onAssetsProgress);
            AssetsManager.loadAssets(Config.DATA.spritesheets);
        }

        public onAssetsProgress=(percent:number)=>
        {
            Logger.log("onAssetsProgress " + percent);

            if (!isNaN(percent))
            {
                percent = Math.round(percent*800)/10;
                this._progress.setText(String(percent) + "%");
            }
        }

        public onAssetsLoaded=()=>
        {
            this._progress.setText("80%");

            SoundManager.init(Config.DATA.audio, Config.IS_MOBILE);
            SignalManager.addCallbackSignal(SoundSignals.SOUNDS_READY, this.onSoundsReady);

            if (Config.IS_MOBILE)
            {
                SoundManager.setMobile();
                this.onSoundsReady();
            }
        }

        private onSoundsReady=()=> {
            SignalManager.removeCallbackSignal(SoundSignals.SOUNDS_READY, this.onSoundsReady);
            this._progress.setText("100%");

            GameStage.goToScreen(ScreenEnum.MAIN);
        }

    }

}
