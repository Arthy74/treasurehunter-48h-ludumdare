module game.signal {
    export class Signals {

        public static DEFAULT_BUTTON_CLICK:string = "default_click";

        // ingame signals
        public static REACHED_EXIT:string = "reached_exit";
    }
}

