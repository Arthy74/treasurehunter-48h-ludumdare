///<reference path="../../lib/PIXI.d.ts" />

///<reference path="../util/DisposeUtil.ts" />

module game {

    // Class
    import DisposeUtil = util.DisposeUtil;

    export class AScreen extends PIXI.DisplayObjectContainer {

        public static COUNT:number = 0;

        public id:string = "ascreen";
        public internalID:number;

        public paused:boolean;
        public built:boolean;

        constructor() {
            super();

            this.internalID = AScreen.COUNT++;

            this.built = false;
            this.paused = false;
        }

        public rescale(sc:number) {
            this.scale = new PIXI.Point(sc, sc);
        }

        public onRemoved() {
            DisposeUtil.dispose(this);
            this.removeChildren();
        }

        public onAdded() {
        }

        public pause() {
            this.paused = true;
        }

        public resume() {
            this.paused = false;
        }

        public isPaused() {
            return this.paused;
        }
    }
}