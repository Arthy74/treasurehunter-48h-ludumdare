///<reference path="../../lib/PIXI.d.ts" />

module log{

    export class Logger {

        private static start:number;
        private static logLayer:PIXI.DisplayObjectContainer;
        private static logText:PIXI.Text;
        private static logBG:PIXI.Graphics;
        private static logHistory:Array<string>;
        private static logHistoryMax:number = 50;

        public static verbose:boolean = false;

        public static init(verbose:boolean) : void
        {
            Logger.verbose = verbose;
            Logger.start = Date.now();
        }

        public static log(... args) : void
        {
            if (!Logger.verbose)
                return;

            var logString = (Date.now()-Logger.start) + ") " + args.toString();
            if (this.logLayer!=null)
            {
                this.logHistory.unshift(logString + "\n");
                if (this.logHistory.length>this.logHistoryMax) this.logHistory.pop();

                this.logText.setText(this.logHistory.toString());
            }
            else
            {
                console.log(logString);
            }
        }

        public static addOnscreenLogger(container:PIXI.DisplayObjectContainer) : void
        {
            this.logLayer = new PIXI.DisplayObjectContainer();
            this.logBG = new PIXI.Graphics();
            this.logLayer.addChild(this.logBG);

            container.addChild(this.logLayer);

            this.logHistory = new Array<string>();

            this.onChangeScale();
        }

        public static onChangeScale() : void
        {
            if (this.logLayer == null) return;
            this.redraw(0,0, Math.round(window.innerWidth/3), Math.round(window.innerHeight));
        }

        private static redraw(xx:number, yy:number, ww:number, hh:number):void{
            if (this.logText != null)
            {
                this.logLayer.removeChild(this.logText);
                this.logText = null;
            }

            this.logBG.clear();
            this.logBG.beginFill(0x000000, 0.5);
            this.logBG.drawRect(xx, yy, ww, hh);

            var font:any = {font: "12px Verdana", fill: "#FFFFFF", align:"left" };
            font.wordWrap = true;
            font.wordWrapWidth = ww-50;

            this.logText = new PIXI.Text("", font);
            this.logText.width = ww;
            //this.logText.height = hh;
            this.logLayer.addChild(this.logText);

            console.log(xx,yy,ww,hh);
        }
    }
}