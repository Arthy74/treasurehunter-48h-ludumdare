///<reference path="../../../lib/PIXI.d.ts" />
///<reference path="../../../lib/tween.js.d.ts" />
///<reference path="../../util/signal/SignalManager.ts" />
///<reference path="../../game/signal/Signals.ts" />
///<reference path="../DisposeUtil.ts" />
///<reference path="./CSS.ts" />

module util.ui {

    import SignalManager = util.signal.SignalManager;
    import Signals = game.signal.Signals;

    export class CommonButton extends PIXI.DisplayObjectContainer {

        public buttonValue = "";
        public buttonFamily : String = "";
        public buttonIndex : number = -1;


        public _selectedSprite:PIXI.Sprite;
        public textureOn : PIXI.Texture;
        public textureOff : PIXI.Texture;
        public tween : TWEEN.Tween;
        public clickData:PIXI.InteractionData;

        public selectedReplaceOver:boolean;

        private _over:PIXI.Sprite;
        private _offExtra:PIXI.Sprite;
        private _selected : boolean;
        private _base:PIXI.Sprite;
        private _clickSignal:string;

        private _wasDown:boolean;
        private _active:boolean;
        private _label:PIXI.Text;

        constructor(baseTxtName:string, overTxtName:string, active:boolean = true) {
            super();

            this._clickSignal = Signals.DEFAULT_BUTTON_CLICK;

            this.tween = new TWEEN.Tween(this);
            this.tween.easing(TWEEN.Easing.Cubic.Out);

            this.textureOn = PIXI.Texture.fromFrame(baseTxtName);

            this.selectedReplaceOver = false;

            this._base = PIXI.Sprite.fromFrame(baseTxtName);
            this._over = PIXI.Sprite.fromFrame(overTxtName);

            this._base.anchor.x = this._base.anchor.y = 0.5;
            this._over.anchor.x = this._over.anchor.y = 0.5;

            this.addChild(this._base);
            this.addChild(this._over);

            this.setActive(active);

            this._over.alpha = 0;
        }

        public disposeLabel()
        {
            if (this._label != null)
            {
                this.removeChild(this._label);
                this._label.destroy(true);
                this._label = null;
            }
        }

        public setLabel(value:string)
        {
            this.disposeLabel();

            if (value == "")
                return;

            var font:any = CSS.getFont(CSS.FONT_Pusab, 50, "#555555");
            CSS.addStroke(font, "#aaaaaa", 4);

            this._label = new PIXI.Text(value, font);
            this.addChild(this._label);

            this._label.anchor.x = this._label.anchor.y = 0.5;
        }

        public dispose():void
        {
            this.setActive(false);
            this.disposeLabel();
            this.clickData = null;
            this._over = null;
            this._selectedSprite = null;
            this._offExtra = null;
            this._base = null;

            if (this.textureOn != null) {
                DisposeUtil.destroyTexture(this.textureOn);
                this.textureOn = null;
            }
            if (this.textureOn != null) {
                DisposeUtil.destroyTexture(this.textureOff);
                this.textureOff = null;
            }

            if (this.tween != null)
                this.tween.stop();
            this.tween = null;
        }

        public setBaseTexture(txt : PIXI.Texture):void{
            this._base.texture = txt;
        }

        public setOverTexture(txt : PIXI.Texture):void{
            this._over.texture = txt;
        }

        public offsetOver(xx:number, yy:number):void{
            this._over.position.x = xx;
            this._over.position.y = yy;
        }

        public setOffTexture(offTxt : PIXI.Texture):void{
            this._offExtra = new PIXI.Sprite(offTxt);
            this._offExtra.anchor.x = this._offExtra.anchor.y = 0.5;
            this.addChild(this._offExtra);
        }
        public offsetOffTexture(xx:number, yy:number):void{
            this._offExtra.position.x = xx;
            this._offExtra.position.y = yy;
        }

        public setSignal(id:string):void{
            this._clickSignal = id;
        }

        public setActive(b:boolean, alterTextures:boolean=true):void {

            if (b) this._active = true;
            else this._active = false;

            this.updateButtonState(b, alterTextures);
        }

        private updateButtonState(b:boolean, alterTextures:boolean=true):void
        {
            this.buttonMode = b;
            this.interactive = b;

            if (b) {
                this.alpha = 1;

                this.mouseover = this.onMouseOver;
                this.mouseout = this.onMouseOut;
                this.mousedown = this.touchstart = this.onMouseDown;
                this.mouseup = this.mouseupoutside = this.touchend = this.touchendoutside = this.onMouseUp;

                if (alterTextures) {
                    if (this.textureOff != null) {
                        this._base.texture = this.textureOn;
                    }
                    if (this._offExtra != null) {
                        this._offExtra.visible = false;
                    }
                }
            } else {
                this.mouseover = null;
                this.mouseout = null;
                this.mousedown = null;
                this.mouseup = this.mouseupoutside = this.touchend = this.touchendoutside = null;

                if (alterTextures) {
                    if (this.textureOff != null) {
                        this._base.texture = this.textureOff;
                    }
                    if (this._offExtra != null) {
                        this._offExtra.visible = true;
                        this.addChild(this._offExtra);
                    }
                }
            }
        }

        public select(b: boolean):void{
            this._selected = b;
            this.updateSelectedState();
        }

        private updateSelectedState():void{
            if (this.selectedReplaceOver)
            {
                this._selectedSprite.alpha = this._selected ? 1 : 0;
                this._base.alpha = this._selected ? 0 : 1;
                this._over.alpha = 0;
            }
            else
            {
                if (this._selected) {
                    if (this._selectedSprite != null) this._selectedSprite.alpha = 1;
                    else if (this._over != null) this._over.alpha = 1;
                } else {
                    if (this._selectedSprite != null) this._selectedSprite.alpha = 0;
                    if (this._over != null) this._over.alpha = 0;
                }
            }
        }

        private onMouseOver(data:PIXI.InteractionData) {
            this.defaultCursor = "pointer";
            data.originalEvent.stopPropagation();

            if (!this._selected) {
                this._over.alpha = 1;
            }
        }

        private onMouseOut(data:PIXI.InteractionData) {
            this.defaultCursor = "pointer";
            if (data.originalEvent!=null) data.originalEvent.stopPropagation();

            this.updateSelectedState();
        }

        private onMouseDown(data:PIXI.InteractionData) {
            this._wasDown = true;
            data.originalEvent.stopPropagation();
            this._over.alpha = 1;
        }

        public onMouseUp = (data:any)=>{
            if (!this._wasDown || data.target != this) return;

            data.originalEvent.stopPropagation();

            this._over.alpha = 0;
            this._wasDown = false;

            this.clickData = data;

            SignalManager.invokeSignal(this._clickSignal, this);
        }
    }
}
