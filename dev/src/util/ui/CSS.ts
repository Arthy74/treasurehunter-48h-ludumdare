///<reference path="../../../lib/PIXI.d.ts" />

module util.ui {
    // Class
    export class CSS {
        public static FONT_CandelaBold : string = "CandelaBold";
        public static FONT_Pusab : string = "Pusab";

        private static styles:Array<Object>;

        public static getFont(id:string, size:number, color:string, align:string="left"):Object  {
            return { font: String(size)+"px "+id, fill: color, align: align };
        }

        public static addDropShadow(obj:any, color:string, angle:number=Math.PI/4, distance:number=3):void  {
            obj.dropShadow = true;
            obj.dropShadowColor = color;
            obj.dropShadowAngle = angle;
            obj.dropShadowDistance = distance;
        }

        public static addStroke(obj:any, color:string, thickness:number):void  {
            obj.stroke = color;
            obj.strokeThickness = thickness;
        }

        public static alignInRect(txt:PIXI.Text, rect : PIXI.Rectangle, alignX:string, alignY:string):void{

            switch (alignX){
                case "center":
                    txt.position.x = Math.round(rect.x + (rect.width-txt.width)/2);
                    break;
                case "left":
                    txt.position.x = Math.round(rect.x);
                    break;
                case "right":
                    txt.position.x = Math.round(rect.x + rect.width - txt.width);
                    break;
            }
            switch (alignY){
                case "middle":
                case "center":
                    txt.position.y = Math.round(rect.y + (rect.height-txt.height)/2);
                    break;
                case "top":
                    txt.position.y = Math.round(rect.y);
                    break;
                case "bottom":
                    txt.position.y = Math.round(rect.y + rect.height - txt.height);
                    break;
            }
        }

    }
}