///<reference path="../../../../lib/howler.d.ts" />
///<reference path="./HowlerSoundVO.ts" />
///<reference path="./HowlerEngine.ts" />

module util.audio.howler {
    // Class

    export class HowlerSound {

        public data : HowlerSoundVO;

        private _soundInstance:any;
        private _engineRef:HowlerEngine;
        private _aSrc:Array<string>;
        private _soundBuilt:boolean;
        private _isMuted:boolean;

        constructor(engine:HowlerEngine, data : HowlerSoundVO) {
            this._engineRef = engine;
            this.data = data;

            this._soundBuilt = false;
            this._isMuted = false;

            this._aSrc = [];
            for (var i = 0, len = data.formats.length; i < len; i++)
            {
                this._aSrc.push(this._engineRef.audioPath + data.url + "." + data.formats[i]);
            }

            if (this.data.preload) this.buildSound();
        }

        private buildSound()
        {
            this._soundInstance = new Howl(
                {
                    src: this._aSrc,
                    autoplay: this.data.autoplay,
                    loop: this.data.loop,
                    volume: this.data.volume / 100,
                    onload: this.onSoundLoaded,
                    onend: this.onSoundComplete
                });

            this._soundBuilt = true;
        }

        private onSoundComplete=()=>
        {
            log.Logger.log("onSoundComplete ", this);
        }

        public stop():void {
            this._soundInstance.stop();
        }

        public play():void {
            if (!this.data.preload && !this._soundBuilt) this.buildSound();
            this._soundInstance.play();

            this.updateMuted();
        }

        public onSoundLoaded=()=>{
            this._engineRef.onSoundPreloaded();
        }

        public mute(b:boolean)
        {
            log.Logger.log("mute ", b);
            this._isMuted = b;
            this.updateMuted();
        }

        private updateMuted():void
        {
            if (this._soundInstance == null) return;

            if (this._isMuted) this._soundInstance.mute(true);
            else this._soundInstance.mute(false);
        }
    }
}