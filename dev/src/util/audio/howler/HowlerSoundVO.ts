module util.audio.howler {
    // Class

    export class HowlerSoundVO {

        public id:string;
        public url:string;
        public preload:boolean;
        public autoplay:boolean;
        public volume:number;
        public loop:boolean;
        public formats:Array<string>;

        constructor(data:any, formats:Array<string>) {
            this.id = data.id;
            this.url = data.url;
            this.preload = data.preload;
            this.autoplay = data.autoplay;
            this.volume = data.volume;
            this.loop = data.loop;
            this.formats = formats;
        }
    }
}