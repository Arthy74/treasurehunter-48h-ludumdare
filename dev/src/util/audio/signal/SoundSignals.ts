module util.audio.signal {
    export class SoundSignals {
        public static SOUND_MUTED:string = "sound muted";
        public static SOUNDS_READY:string = "sounds ready";
    }
}

