///<reference path="../signal/SignalManager.ts" />
///<reference path="../signal/CoreSignals.ts" />
///<reference path="../Log.ts" />

module util.io {
    // Class

    import Logger = log.Logger;
    import SignalManager = util.signal.SignalManager;
    import CoreSignals = util.signal.CoreSignals;

    export class IORequest {

        public silenceMode:boolean;

        private _request:XMLHttpRequest;
        private _success:Function;
        private _fail:Function;

        private _method : string;
        private _url : string;
        private _data : Object;

        constructor(url:string, method:string="get", data:Object = null) {
            this.silenceMode = false;

            this._method = method;
            this._url = url;
            this._data = data;

            this._request = new XMLHttpRequest();
            this._request.onload = this.onGetAnswer;
            this._request.onerror = this.onError;
            this._request.ontimeout = this.onError;

            Logger.log(this._request);
            this._request.open(this._method, this._url, true);
        }

        public sendRequest(cbSuccess : Function, cbfail:Function=null):void{
            if (!this.silenceMode) {
                SignalManager.invokeSignal(CoreSignals.SHOW_REQUEST_SHIELD);
            }

            this._success = cbSuccess;
            this._fail = cbfail;

            if (this._method == "get"){
                log.Logger.log("send request to ", this._url, "GET");
                this._request.send();
            }

            if (this._method == "post"){
                var data : string = JSON.stringify(this._data);
                log.Logger.log("send request to ", this._url, "POST", data);
                this._request.send(data);
            }
        }

        public onGetAnswer = (evt : Event)=>{
            var json:string = JSON.parse(this._request.responseText);

            if (!this.silenceMode)
            {
                SignalManager.invokeSignal(CoreSignals.HIDE_REQUEST_SHIELD);
            }

            if (this._success != null) this._success.call(null, json);
        }

        public onError = (evt : Event)=>{
            log.Logger.log("---> ON ERROR ", evt);
            if (!this.silenceMode)
            {
                SignalManager.invokeSignal(CoreSignals.HIDE_REQUEST_SHIELD);
            }
            if (this._fail != null)
                this._fail.call(null, evt.type);
        }
    }
}