///<reference path="SignalCallback.ts" />

module util.signal {

    // Class
    export class ASignal {

        public id : string;
        public callbacks : Array<SignalCallback>;
        public verbose : boolean;

        private _idx : number;


        constructor(id_message : string, verbose : boolean = false) {
            this.callbacks = new Array<SignalCallback>();

            this.id = id_message;
            this.verbose = verbose;
        }

        /**
         * Disposes this message
         */
        public dispose() : void
        {
            this.callbacks = new Array<SignalCallback>();
            this.callbacks = null;
        }

        public addCallback( cb : Function, priority : number = 0 ) : void
        {
            this._idx = this.getItemIndex( cb );

            if( this._idx == -1 )
            {
                this.callbacks.push( new SignalCallback(cb, priority) );

                this.callbacks.sort( this.sortCallBack );
            }
        }

        private sortCallBack(a:SignalCallback, b:SignalCallback ):number{
            if (a.priority<b.priority) return 1;
            else if (a.priority>b.priority) return -1;
            return 0;
        }

         public removeCallback( cb : Function ) : void
        {
            //log.Logger.log("remove ", cb);
            this._idx = this.getItemIndex( cb );

           // log.Logger.log("idx ", this._idx);

            if( this._idx != -1 )
            {
                this.callbacks.splice( this._idx, 1 );
            }
        }

        /**
         * remove all callbacks associated to this message
         */
        public removeALLCallbacks() : void
        {
            this.callbacks = new Array<SignalCallback>();
        }

        public exists( cb : Function ) : boolean
        {
            return ( this.getItemIndex( cb ) != -1 );
        }

        /**
         *	Calls all associated callbacks with provided arguments
         *
         * @param args	arguments to be sent to each callback
         */
        public invokeCallbacks( args : Array<SignalCallback> ) : void
        {
            if (this.verbose) log.Logger.log( "invoking callbacks '" + this.id + "'  nb:" + this.callbacks.length + "  nb args:" + args.length );

            var a : Array<SignalCallback> = this.callbacks.concat();
            var l : number = a.length;
            var cb : Function;

            for( var i : number = 0; i < l; i++ )
            {
                cb = a[i].cb;

                if( cb == null )
                {
                    log.Logger.log( "WARNING on " + this.id + ": callback may be not available anymore or callback signature is not the same (number of arguments expected:" + args.length + "). Removing callback." );
                    this.removeCallback( cb );
                    continue;
                }

                switch( args.length )
                {
                    case 0:
                        cb.call( null );
                        break;
                    case 1:
                        cb.call( null, args[ 0 ] );
                        break;
                    case 2:
                        cb.call( null, args[ 0 ], args[ 1 ] );
                        break;
                    case 3:
                        cb.call( null, args[ 0 ], args[ 1 ], args[ 2 ] );
                        break;
                    case 4:
                        cb.call( null, args[ 0 ], args[ 1 ], args[ 2 ], args[ 3 ] );
                        break;
                    case 5:
                        cb.call( null, args[ 0 ], args[ 1 ], args[ 2 ], args[ 3 ], args[ 4 ] );
                        break;
                    case 6:
                        cb.call( null, args[ 0 ], args[ 1 ], args[ 2 ], args[ 3 ], args[ 4 ],
                            args[ 5 ] );
                        break;
                    case 7:
                        cb.call( null, args[ 0 ], args[ 1 ], args[ 2 ], args[ 3 ], args[ 4 ],
                            args[ 5 ], args[ 6 ] );
                        break;
                    case 8:
                        cb.call( null, args[ 0 ], args[ 1 ], args[ 2 ], args[ 3 ], args[ 4 ],
                            args[ 5 ], args[ 6 ], args[ 7 ] );
                        break;
                    case 9:
                        cb.call( null, args[ 0 ], args[ 1 ], args[ 2 ], args[ 3 ], args[ 4 ],
                            args[ 5 ], args[ 6 ], args[ 7 ], args[ 8 ] );
                        break;
                    case 10:
                        cb.call( null, args[ 0 ], args[ 1 ], args[ 2 ], args[ 3 ], args[ 4 ],
                            args[ 5 ], args[ 6 ], args[ 7 ], args[ 8 ], args[ 9 ] );
                        break;
                    case 11:
                        cb.call( null, args[ 0 ], args[ 1 ], args[ 2 ], args[ 3 ], args[ 4 ],
                            args[ 5 ], args[ 6 ], args[ 7 ], args[ 8 ], args[ 9 ], args[ 10 ] );
                        break;
                    default:
                }
            }
        }

        public toString() : String
        {
            return "nb of callbacks for message '" + this.id + "' : " + this.callbacks.length;
        }

        private getItemIndex( ref : Function ):number{
            var idx : number = -1;
            var l : number = this.callbacks.length;
            for (var i:number = 0; i < l; i++)
            {
                if (this.callbacks[i].cb == ref){
                    return i;
                    break;
                }
            }
            return idx;
        }
    }
}